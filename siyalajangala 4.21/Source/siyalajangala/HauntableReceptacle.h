// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HauntableDoodad.h"
#include "Components/StaticMeshComponent.h"
#include "HauntableReceptacle.generated.h"

class APickableDoodad;

UCLASS()
class SIYALAJANGALA_API AHauntableReceptacle : public AHauntableDoodad
{
	GENERATED_BODY()
	
public:
	AHauntableReceptacle();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(ReplicatedUsing = OnRep_FragmentNum)
		int FragmentNum = 1;
	UPROPERTY(ReplicatedUsing = OnRep_Key)
		bool bKey = false;
	UPROPERTY(ReplicatedUsing = OnRep_Activated)
		bool bActivated = false;
	bool bDoActionAnimation = false;

	FVector KeyBasePosition;
	UPROPERTY(EditAnywhere)
		float FloatingKeyAnimTime = 5.0f;
	float FloatingKeyAnimTimeCount = 0.0f;
	UPROPERTY(EditAnywhere)
		FVector FloatingKeyAnimAxis = FVector(0, 0, 20);

	FRotator KeyBaseRotation;
	UPROPERTY(EditAnywhere)
		float MinRotatingSpeed = 0.0f;
	UPROPERTY(EditAnywhere)
		float MaxRotatingSpeed = 0.04f;
	UPROPERTY(EditAnywhere)
		FRotator RotatingKeyAnimAxis = FRotator(0, 1, 0);

	UPROPERTY(EditAnywhere)
		float ApparitionAnimTime = 10.0f;
	float ApparitionAnimTimeCount = 0.0f;
	FTimerHandle ApparitionTimerHandle;
	int LastAppearedStair = -1;
	TArray<UStaticMeshComponent*> Stairs;

	TArray<UStaticMeshComponent*> Fragments;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* BaseMesh;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* FragmentMesh1;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* FragmentMesh2;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* FragmentMesh3;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* KeyMesh;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh1;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh2;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh3;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh4;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh5;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh6;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh7;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh8;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh9;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh10;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh11;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StairwayToHeavenMesh12;

	UFUNCTION()
		void OnRep_FragmentNum();
	UFUNCTION()
		void OnRep_Key();
	UFUNCTION()
		void OnRep_Activated();

	UFUNCTION()
		void MakeAppearOneStair();
public:
	UFUNCTION(Server, Reliable, WithValidation)
	void Place(APickableDoodad* Item);
	virtual void Tick(float DeltaTime) override;
	virtual void Action_Implementation() override;

	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;
};
