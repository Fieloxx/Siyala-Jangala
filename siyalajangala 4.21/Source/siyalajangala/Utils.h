#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Jhada.h"
#include "Spirit.h"
#include "Engine.h"

class Utils
{
public:

	template<typename TEnum>
	static FORCEINLINE FString GetEnumValueAsString(const FString& Name, TEnum Value)
	{
		const UEnum* enumPtr = FindObject<UEnum>(ANY_PACKAGE, *Name, true);
		if (!enumPtr)
		{
			return FString("Invalid");
		}
		return enumPtr->GetNameByValue((int64)Value).ToString();
	}
	// Example usage
	// GetEnumValueAsString<EVictoryEnum>("EVictoryEnum", VictoryEnum)));

	template <typename EnumType>
	static FORCEINLINE EnumType GetEnumValueFromString(const FString& EnumName, const FString& String)
	{
		UEnum* Enum = FindObject<UEnum>(ANY_PACKAGE, *EnumName, true);
		if (!Enum)
		{
			return EnumType(0);
		}
		return (EnumType)Enum->GetValueByName(FName(*String));
	}
	//Sample Usage
	// FString ParseLine = GetEnumValueAsString<EChallenge>("EChallenge", VictoryEnumValue))); //To String
	// EChallenge Challenge = GetEnumValueFromString<EChallenge>("EChallenge", ParseLine);  //Back From String!

	static FORCEINLINE void SetOwnerToSpirit(AActor* Actor) {
		TArray<AActor*> Spirits;
		
		UGameplayStatics::GetAllActorsOfClass(Actor->GetWorld(), ASpirit::StaticClass(), Spirits);
		if (Spirits.Num() > 0) {
			Actor->SetOwner(Spirits[0]->GetOwner());
		}
		else {
			if (GEngine) {

			}
			UE_LOG(LogTemp, Warning, TEXT("No Spirit owner was found."));
		}
	}

	static FORCEINLINE void SetOwnerToJhada(AActor* Actor) {
		TArray<AActor*> Jhadas;
		UGameplayStatics::GetAllActorsOfClass(Actor->GetWorld(), AJhada::StaticClass(), Jhadas);
		if (Jhadas.Num() > 0) {
			Actor->SetOwner(Jhadas[0]->GetOwner());
		}
		else {
			if (GEngine) {

			}
			UE_LOG(LogTemp, Warning, TEXT("No Jhada owner was found."));
		}
	}

	static FORCEINLINE AJhada* GetJhada(UWorld* World) {
		TArray<AActor*> Jhadas;
		UGameplayStatics::GetAllActorsOfClass(World, AJhada::StaticClass(), Jhadas);
		if (Jhadas.Num() > 0) {
			return Cast<AJhada>(Jhadas[0]);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("No Jhada was found."));
			return nullptr;
		}
	}

	static FORCEINLINE ASpirit* GetSpirit(UWorld* World) {
		TArray<AActor*> Spirits;
		UGameplayStatics::GetAllActorsOfClass(World, AJhada::StaticClass(), Spirits);
		if (Spirits.Num() > 0) {
			return Cast<ASpirit>(Spirits[0]);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("No Spirit was found."));
			return nullptr;
		}
	}
};

