// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Doodad.h"
#include "ConcreteDoodad.generated.h"

UCLASS()
class SIYALAJANGALA_API AConcreteDoodad : public AActor, public IDoodad
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AConcreteDoodad();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Enum, Replicated)
	ECorruptionState CorruptionState;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual ECorruptionState GetCorruption() const override;

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void SetCorruption(ECorruptionState c_state) override;

	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;
};
