#pragma once

#include "ConcreteDoodad.h"
#include "Jhada.h"
#include "Camera/CameraComponent.h"
#include "PickableDoodad.generated.h"


UCLASS()
class SIYALAJANGALA_API APickableDoodad : public AConcreteDoodad {
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	APickableDoodad();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Replicated)
	UStaticMeshComponent* DoodadMesh;

	UPROPERTY(EditAnywhere)
	USceneComponent* HoldingComp;

	/**
	* D�finit l'objet comme pris. Ou au contraire comme rel�ch�. DOIT �TRE REPLIQUE !
	**/
	UFUNCTION(Server, Reliable, WithValidation)
	virtual void PickUp();

	/**
	* WARNING : Those functions are used in alternation in PickUp and should not be used outside.
	* But they can be override.
	**/
	UFUNCTION(Server, Reliable, WithValidation)
	virtual void Take();
	UFUNCTION(Server, Reliable, WithValidation)
	virtual void Release();

	UPROPERTY(Replicated)
	bool bHolding;
	UPROPERTY(Replicated)
	bool bGravity;
	UPROPERTY(Replicated)
	ACharacter* Jhada;

	UCameraComponent* PlayerCamera;
	FVector ForwardVector;

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void SetHolding(bool NbHolding);
	UFUNCTION(Server, Reliable, WithValidation)
	virtual void SetGravity(bool NbGravity);
	
	virtual void SetJhada(ACharacter* NJhada);
	UFUNCTION(Server, Reliable, WithValidation)
	virtual void SetJhada_Server(ACharacter* NJhada);


	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;
};
