// Fill out your copyright notice in the Description page of Project Settings.

#include "SJ_PlayerController.h"
#include "UnrealNetwork.h"

ASJ_PlayerController::ASJ_PlayerController() : APlayerController() {
	Spirit = nullptr;
	PossessedDoodad = nullptr;
	
	//SetReplicates(true);
	//SetReplicateMovement(true);
}

bool ASJ_PlayerController::PossessDoodad_Validate(AHauntableDoodad* Doodad) {
	return true;
}

void ASJ_PlayerController::PossessDoodad_Implementation(AHauntableDoodad* Doodad) {
	

	Spirit = Cast<ASpirit>(GetPawn());

	if (Spirit->GetClass()->IsChildOf(ASpirit::StaticClass()) && Doodad) {
		if (Doodad->GetCorruption() == ECorruptionState::CS_None) {
			UnPossess();
			PossessedDoodad = Doodad;
			Doodad->SetOwner(this);
			PossessedDoodad->SetPossessed(true);
			Possess(Doodad);

			if (GEngine) {
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Possession ongoing."));
			}
		} else {
			if (GEngine) {
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Doodad is corrupted."));
			}
		}
	} else {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Not a spirit / Not a doodad"));
		}
	}
}

bool ASJ_PlayerController::UnpossessDoodad_Validate() {
	return true;
}

void ASJ_PlayerController::UnpossessDoodad_Implementation() {
	if (!Spirit) {
		return;
	}

	RelocateSpiritBeforeUnpossess();

	PossessedDoodad->SetPossessed(false);
	PossessedDoodad = nullptr;
	UnPossess();

	Possess(Spirit);
	Spirit->SetPossessing(false);
	Spirit = nullptr;
}

bool ASJ_PlayerController::RelocateSpiritBeforeUnpossess_Validate() {
	return true;
}

void ASJ_PlayerController::RelocateSpiritBeforeUnpossess_Implementation() {
	FVector DoodadLocation = PossessedDoodad->GetActorLocation();
	Spirit->RelocateTo(DoodadLocation);
}

void ASJ_PlayerController::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ASJ_PlayerController, Spirit);
	DOREPLIFETIME(ASJ_PlayerController, PossessedDoodad);
}