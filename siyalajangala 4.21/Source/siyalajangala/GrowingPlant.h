#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "GrowingPlant.generated.h"

class APickableDoodad;
class AHauntableDoodad;
class APickableCristal;
class APickableSeed;
class AHauntableWater;

UCLASS()
class SIYALAJANGALA_API AGrowingPlant : public AActor {
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGrowingPlant();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(ReplicatedUsing = OnRep_Seed)
	bool bSeed;
	UPROPERTY(ReplicatedUsing = OnRep_Water)
	bool bWater;
	UPROPERTY(ReplicatedUsing = OnRep_Grown)
	bool bGrown;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* EarthMesh;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* SeedMesh;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* WaterMesh;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PlantMesh1;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PlantMesh2;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PlantMesh3;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PlantMesh4;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PlantMesh5;

	UPROPERTY(EditAnywhere)
		APickableCristal* Cristal;

	UFUNCTION()
	virtual void OnRep_Seed();

	UFUNCTION()
	virtual void OnRep_Water();

	UFUNCTION()
	virtual void OnRep_Grown();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Server, Reliable, WithValidation)
	void PlaceSeed(APickableSeed* Seed);
	UFUNCTION(Server, Reliable, WithValidation)
	void PlaceWater(AHauntableWater* Water);
	UFUNCTION(Server, Reliable, WithValidation)
	void GrowPlant();

	bool IsGrown() const;
	bool HasSeed() const;
	bool IsWatered() const;


	virtual void GetLifetimeReplicatedProps(class TArray<class FLifetimeProperty, class FDefaultAllocator>& OutLifetimeProps) const override;
};
