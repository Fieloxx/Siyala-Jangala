// Fill out your copyright notice in the Description page of Project Settings.

#include "Portal.h"
#include "PickableDoodad.h"
#include "PickableRunestoneJhada.h"
#include "PickableRunestoneSpirit.h"
#include "Components/StaticMeshComponent.h"
#include "UnrealNetwork.h"

// Sets default values
APortal::APortal(): AActor() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FrameMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FrameMesh"));
	// FrameMesh->SetupAttachment(RootComponent);
	SetRootComponent(FrameMesh);

	SpiritRuneMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SpiritRuneMesh"));
	SpiritRuneMesh->SetupAttachment(FrameMesh);

	JhadaRuneMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("JhadaRuneMesh"));
	JhadaRuneMesh->SetupAttachment(FrameMesh);

	PortalMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PortalMesh"));
	PortalMesh->SetupAttachment(FrameMesh);
	PortalMesh->SetSimulatePhysics(false);
	PortalMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PortalMesh->SetGenerateOverlapEvents(false);

	isActivated = false;

	SetReplicates(true);
}

// Called when the game starts or when spawned
void APortal::BeginPlay() {
	Super::BeginPlay();
	FrameMesh->SetVisibility(true);
	SpiritRuneMesh->SetVisibility(false);
	JhadaRuneMesh->SetVisibility(false);
	PortalMesh->SetVisibility(false);
}

// Called every frame
void APortal::Tick(float DeltaTime) {
	AActor::Tick(DeltaTime);
}

bool APortal::PlaceRunestone_Validate(APickableDoodad* Runestone) {
	return true;
}

void APortal::PlaceRunestone_Implementation(APickableDoodad* Runestone) {
	if (Runestone->GetClass()->IsChildOf(APickableRunestoneJhada::StaticClass())) {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Rune is Jhada's"));
		}
		MakeJhadaRuneAppear();
	}
	else if (Runestone->GetClass()->IsChildOf(APickableRunestoneSpirit::StaticClass())) {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Rune is Spirit's"));
		}
		MakeSpiritRuneAppear();
	}
	Runestone->Destroy();

	if (JhadaRuneMesh->IsVisible() && SpiritRuneMesh->IsVisible()) {
		ActivatePortalMesh();
	}
}

bool APortal::MakeJhadaRuneAppear_Validate() {
	return true;
}

void APortal::MakeJhadaRuneAppear_Implementation() {
	JhadaRuneMesh->SetVisibility(true);
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Jhada rune appears"));
	}
}

bool APortal::MakeSpiritRuneAppear_Validate() {
	return true;
}

void APortal::MakeSpiritRuneAppear_Implementation() {
	SpiritRuneMesh->SetVisibility(true);
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Spirit rune appears"));
	}
}

bool APortal::ActivatePortalMesh_Validate() {
	return true;
}

void APortal::ActivatePortalMesh_Implementation() {
	PortalMesh->SetVisibility(true);
	PortalMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	PortalMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	PortalMesh->SetGenerateOverlapEvents(true);
	isActivated = true;
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Portal is activated"));
	}
}

void APortal::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}