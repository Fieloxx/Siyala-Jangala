#include "BriarBush.h"
#include "UnrealNetwork.h"
#include "Utils.h"


// Sets default values
ABriarBush::ABriarBush() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetReplicates(true);
	bReplicates = true;

	BushMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bush Mesh 1"));
	SetRootComponent(BushMesh1);

	BushMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bush Mesh 2"));
	BushMesh2->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABriarBush::BeginPlay() {
	AActor::BeginPlay();
}

// Called every frame
void ABriarBush::Tick(float DeltaTime) {
	AActor::Tick(DeltaTime);

}

bool ABriarBush::MoveBush_Validate() {
	return true;
}

void ABriarBush::MoveBush_Implementation() {
	MoveBush_Multicast();
}

bool ABriarBush::MoveBush_Multicast_Validate() {
	return true;
}

void ABriarBush::MoveBush_Multicast_Implementation() {
	if (bMoveOnce) {
		BushMesh1->SetWorldLocation(BushMesh1->GetComponentLocation() + MoveVector1);
		BushMesh2->SetWorldLocation(BushMesh2->GetComponentLocation() + MoveVector2);
		bMoveOnce = false;
	}
}

void ABriarBush::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}