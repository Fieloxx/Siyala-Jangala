// Fill out your copyright notice in the Description page of Project Settings.

#include "PickableScalingFruit.h"
#include "UnrealNetwork.h"
#include "Engine/Public/TimerManager.h"

APickableScalingFruit::APickableScalingFruit() : APickableDoodad() {

}

void APickableScalingFruit::BeginPlay() {
	Super::BeginPlay();

	Scale = GetActorScale3D();
	SetScale(GetActorScale3D());
}

void APickableScalingFruit::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (bHolding && HoldingComp) {
		SetActorLocationAndRotation(HoldingComp->GetComponentLocation() + (PlayerCamera->GetForwardVector() * 10), HoldingComp->GetComponentRotation());
	}
}

void APickableScalingFruit::Take_Implementation() {
	Super::Take_Implementation();
	SetJhadaScale(1.0f/ScaleAmount);
}

void APickableScalingFruit::Release_Implementation() {
	FVector JhadaBoxExtent; // HalfBoundingBox
	FVector JhadaCenter;
	Jhada->GetActorBounds(true, JhadaCenter, JhadaBoxExtent);
	FHitResult HitResult;
	// Avoid collision while becoming bigger by placing at center.
	SetActorLocation(Jhada->GetActorLocation() + FVector(0, 0, JhadaBoxExtent.Z*ScaleAmount), true, &HitResult); 
	bool bCanGrow;
	if (HitResult.bBlockingHit) {
		bCanGrow = false;
	}
	else {
		// On v�rifie que Jhada rentre avec sa taille compl�te
		SetActorLocation(Jhada->GetActorLocation() + FVector(0, 0, JhadaBoxExtent.Z*ScaleAmount - JhadaBoxExtent.Z), true, &HitResult);
		bCanGrow = !HitResult.bBlockingHit;
	}
	
	if (bCanGrow) {
		SetJhadaScale(1.0f);

		// Update position after the scale
		SetActorLocationAndRotation(HoldingComp->GetComponentLocation() + (PlayerCamera->GetForwardVector() * 10), HoldingComp->GetComponentRotation());
		Super::Release_Implementation();
	}
}

bool APickableScalingFruit::SetScale_Validate(FVector NScale) {
	return true;
}

void APickableScalingFruit::SetScale_Implementation(FVector NScale) {
	Scale = NScale;
}

bool APickableScalingFruit::SetJhadaScale_Validate(float ScaleFactor) {
	return true;
}

void APickableScalingFruit::SetJhadaScale_Implementation(float ScaleFactor) {
	Jhada->SetActorScale3D(ScaleFactor * FVector(1, 1, 1));
}

void APickableScalingFruit::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APickableScalingFruit, Scale);
}