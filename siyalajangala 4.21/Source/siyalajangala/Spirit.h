#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Spirit.generated.h"

class AHauntableDoodad;
class AHauntableLantern;

UCLASS()
class SIYALAJANGALA_API ASpirit : public ACharacter {
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASpirit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		USpringArmComponent* SpringArm;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UCameraComponent* Camera;

	UPROPERTY(EditAnywhere)
		UCapsuleComponent* Capsule;
	
	FVector2D MovementInput;
	FVector2D CameraInput;
	float ZoomFactor;
	bool bZoomingIn;

	UPROPERTY(EditAnywhere, Category = Gameplay)
		float minHeight;
	UPROPERTY(EditAnywhere, Category = Gameplay)
		float maxHeight;

	UPROPERTY(Replicated)
	AHauntableDoodad* CurrentItem;
	UPROPERTY(Replicated)
	AHauntableLantern* Lantern = nullptr;
	UPROPERTY(Replicated)
	bool bPossessing = false;

	// To create the Hit vector, to see what actor we are aiming for
	FVector Start;
	FVector ForwardVector;
	FVector End;
	FHitResult Hit;
	FComponentQueryParams ComponentQueryParams;
	FCollisionResponseParams ResponseParam;

	

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Handles input for moving forward and backward.
	void MoveForward(float AxisValue);
	// Handles input for moving right and left.
	void MoveRight(float AxisValue);
	// Rotate camera up and down
	void PitchCamera(float AxisValue);

	void RelocateTo(const FVector& Location);

	/**
	* Used in teleportTo
	**/
	UFUNCTION()
	void ReactivateCameraLag();

	void ZoomIn();
	void ZoomOut();

	void GoUp();
	void GoDown();

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void Purify();

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void Possess();

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void SetPossessing(bool p);

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void SetCurrentItem(AHauntableDoodad* NCurrentItem);

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void SetLantern(AHauntableLantern* NCurrentItem);

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;
};
