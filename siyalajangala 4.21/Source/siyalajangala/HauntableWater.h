#pragma once

#include "CoreMinimal.h"
#include "FlyingHauntableDoodad.h"
#include "GameFramework/Character.h"
#include "HauntableWater.generated.h"

class AGrowingPlant;
class AFIllableWell;

UCLASS()
class SIYALAJANGALA_API AHauntableWater : public AFlyingHauntableDoodad {
	GENERATED_BODY()
	
public:
	AHauntableWater();

protected:
	virtual void BeginPlay() override;

	FVector Start;
	FVector ForwardVector;
	FVector End;
	FHitResult Hit;
	FComponentQueryParams DefaultComponentQueryParams;
	FCollisionResponseParams DefaultResponseParams;

	UPROPERTY(Replicated)
	AGrowingPlant* CurrentItem;
	UPROPERTY(Replicated)
	AFIllableWell* CurrentWell;
	UPROPERTY(ReplicatedUsing = OnRep_HasBeenPossessed)
		bool bHasBeenPossessed = false;
	
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PossessCollisionMesh;

	UPROPERTY(EditAnywhere, Category = Gameplay)
		float minHeight;
	UPROPERTY(EditAnywhere, Category = Gameplay)
		float maxHeight;

public:
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void Action_Implementation() override;

	virtual void SetCurrentItem(AGrowingPlant* NCurrentItem);
	UFUNCTION(Server, Reliable, WithValidation)
	virtual void SetCurrentItem_Server(AGrowingPlant* NCurrentItem);

	virtual void SetCurrentWell(AFIllableWell* NCurrentWell);
	UFUNCTION(Server, Reliable, WithValidation)
	virtual void SetCurrentWell_Server(AFIllableWell* NCurrentWell);

	virtual void OnRep_PossessionChanged() override;

	UFUNCTION()
	virtual void OnRep_HasBeenPossessed();

	virtual void GetLifetimeReplicatedProps(class TArray<class FLifetimeProperty, class FDefaultAllocator>& OutLifetimeProps) const override;
};
