#pragma once

#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "Jhada.generated.h"

UCLASS()
class SIYALAJANGALA_API AJhada : public ACharacter {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Replicated)
		class USceneComponent* HoldingComponent;
	// FPS camera.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UCameraComponent* JhadaCameraComponent;
	// First-person mesh (arms), visible only to the owning player.
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		USkeletalMeshComponent* JhadaFPSMesh;
	UPROPERTY(EditAnywhere, Category = Mesh)
		UStaticMeshComponent* SmellMesh;

	// Sets default values for this character's properties
	AJhada();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector2D MovementInput;

	UPROPERTY(Replicated)
		bool bPressedSmell = false;
	UPROPERTY(ReplicatedUsing = OnRep_HoldingItem)
		class APickableDoodad* HoldingItem = nullptr;

	FVector HoldingComp;
	FVector Start;
	FVector ForwardVector;
	FVector End;
	FHitResult Hit;
	FComponentQueryParams DefaultComponentQueryParams;
	FCollisionResponseParams DefaultResponseParams;

	UPROPERTY(Replicated)
		bool bPortal;
	UPROPERTY(Replicated)
		bool bGrowingPlant;
	UPROPERTY(Replicated)
		bool bBriarBush;
	UPROPERTY(Replicated)
		bool bReceptacle;
	UPROPERTY(ReplicatedUsing = OnRep_HasSmelled)
		bool bHasSmelled = false; // Has Jhada smelled at least once in the game.

		// [NETWORK]
	float Health;
	UPROPERTY(EditAnywhere)
		float MaxHealth = 3;
	bool bHurting = false;
	FVector SafeLocation;
	// [NETWORK]

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// [NETWORK]
	virtual float TakeDamage(float DamageAmount, const struct FDamageEvent& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser);

	// Handles input for moving forward and backward.
	UFUNCTION()
		void MoveForward(float Value);

	// Handles input for moving right and left.
	UFUNCTION()
		void MoveRight(float Value);

	// Sets jump flag when key is pressed.
	UFUNCTION()
		void StartJump();

	// Clears jump flag when key is released.
	UFUNCTION()
		void StopJump();

	UFUNCTION()
		void StartSmelling();

	UFUNCTION()
		void StopSmelling();

	UFUNCTION(Server, Reliable, WithValidation)
		void Purify();

	UPROPERTY(ReplicatedUsing = OnRep_CurrentItem) // TODO For the others too, if this works
		class APickableDoodad* CurrentItem; // Is the viewed item
	UPROPERTY(Replicated)
		class APortal* Portal;
	UPROPERTY(Replicated)
		class AGrowingPlant* GrowingPlant;
	UPROPERTY(Replicated)
		class ABriarBush* BriarBush;
	UPROPERTY(Replicated)
		class AHauntableReceptacle* Receptacle;
	UPROPERTY(Replicated)
		class AHauntableLantern* Lantern = nullptr;

	UPROPERTY(EditAnywhere)
		AActor* Smellable;

public:
	void ToggleItemPickup();
	UFUNCTION(Server, Reliable, WithValidation)
		void ToggleItemPickup_Server();

	// Those functions should be called inside server rpc only.
	virtual void TakeItem();
	virtual void LeaveItem();


	UFUNCTION(Server, Reliable, WithValidation)
		void SetCurrentItem(class APickableDoodad* NCurrentItem);
	UFUNCTION(Server, Reliable, WithValidation)
		void SetPortal(class APortal* NPortal);
	UFUNCTION(Server, Reliable, WithValidation)
		void SetGrowingPlant(class AGrowingPlant* NGrowingPlant);
	UFUNCTION(Server, Reliable, WithValidation)
		void SetBriarBush(class ABriarBush* NBriarBush);
	UFUNCTION(Server, Reliable, WithValidation)
		void SetReceptacle(class AHauntableReceptacle* NReceptacle);
	UFUNCTION(Server, Reliable, WithValidation)
		void SetLantern(class AHauntableLantern* NLantern);
	UFUNCTION(Server, Reliable, WithValidation)
		void SetHasSmelled(bool NbHasSmell);

	UFUNCTION()
		void OnRep_HasSmelled();
	UFUNCTION()
		void OnRep_HoldingItem();
	UFUNCTION()
		void OnRep_CurrentItem();

	void DisplaySmell(bool bDisplayed);
	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;


};
