#include "HauntableWater.h"
#include "GrowingPlant.h"
#include "FIllableWell.h"
#include "SJ_PlayerController.h"
#include "DrawDebugHelpers.h"
#include "UnrealNetwork.h"

AHauntableWater::AHauntableWater() : Super() {
	CurrentItem = nullptr;
	CurrentWell = nullptr;

	PossessCollisionMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PossessCollisionMesh"));
	PossessCollisionMesh->SetupAttachment(RootComponent);
	
	DoodadMesh->SetVisibility(false);
}

void AHauntableWater::BeginPlay() {
	Super::BeginPlay();

	DoodadMesh->SetVisibility(false);

	PossessCollisionMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	PossessCollisionMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
}

void AHauntableWater::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (bPossessed && IsLocallyControlled()) {
		//DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);

		Start = Camera->GetComponentLocation();
		ForwardVector = Camera->GetForwardVector();
		End = (ForwardVector * 2000.0f) + Start;

		SetActorEnableCollision(false);
		if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
			if (Hit.GetActor()->GetClass()->IsChildOf(AGrowingPlant::StaticClass())) {
				SetCurrentItem(Cast<AGrowingPlant>(Hit.GetActor()));
				SetCurrentWell(nullptr);
			} else if (Hit.GetActor()->GetClass()->IsChildOf(AFIllableWell::StaticClass())) {
				SetCurrentWell(Cast<AFIllableWell>(Hit.GetActor()));
				SetCurrentItem(nullptr);
			} else {
				SetCurrentItem(nullptr);
				SetCurrentWell(nullptr);
			}
		} else {
			SetCurrentItem(nullptr);
			SetCurrentWell(nullptr);
		}
		SetActorEnableCollision(true);
	}
}

void AHauntableWater::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AHauntableWater::Action_Implementation() {
	if (CurrentItem) {
		Unpossess();
		CurrentItem->PlaceWater(this);
		Destroy();
	} else if (CurrentWell) {
		Unpossess();
		CurrentWell->Fill(this);
		Destroy();
	}
}

void AHauntableWater::SetCurrentItem(AGrowingPlant* NCurrentItem) {
	if (NCurrentItem != CurrentItem) {
		SetCurrentItem_Server(NCurrentItem);
	}
}

bool AHauntableWater::SetCurrentItem_Server_Validate(AGrowingPlant* NCurrentItem) {
	return true;
}

void AHauntableWater::SetCurrentItem_Server_Implementation(AGrowingPlant* NCurrentItem) {
	CurrentItem = NCurrentItem;
}

bool AHauntableWater::SetCurrentWell_Server_Validate(AFIllableWell* NCurrentWell) {
	return true;
}

void AHauntableWater::SetCurrentWell_Server_Implementation(AFIllableWell* NCurrentWell) {
	CurrentWell = NCurrentWell;
}

void AHauntableWater::SetCurrentWell(AFIllableWell* NCurrentWell) {
	if (NCurrentWell != CurrentWell) {
		SetCurrentWell_Server(NCurrentWell);
	}
}

void AHauntableWater::OnRep_PossessionChanged() {
	Super::OnRep_PossessionChanged();

	if (bPossessed && !bHasBeenPossessed) {
		bHasBeenPossessed = true;
		OnRep_HasBeenPossessed();
	}
}

void AHauntableWater::OnRep_HasBeenPossessed() {
	if (bHasBeenPossessed) {
		DoodadMesh->SetVisibility(true);
		PossessCollisionMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	else {
		DoodadMesh->SetVisibility(false);
		PossessCollisionMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
}


void AHauntableWater::GetLifetimeReplicatedProps(class TArray<class FLifetimeProperty, class FDefaultAllocator>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AHauntableWater, CurrentItem);
	DOREPLIFETIME(AHauntableWater, CurrentWell);
	DOREPLIFETIME(AHauntableWater, bHasBeenPossessed);
}