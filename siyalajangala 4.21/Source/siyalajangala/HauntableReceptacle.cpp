// Fill out your copyright notice in the Description page of Project Settings.

#include "HauntableReceptacle.h"
#include "PickableDoodad.h"
#include "PickableFragment.h"
#include "PickableKey.h"
#include "UnrealNetwork.h"
#include "Engine.h"

AHauntableReceptacle::AHauntableReceptacle() {
	PrimaryActorTick.bCanEverTick = true;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh"));
	RootComponent = BaseMesh;

	FragmentMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Fragment 1"));
	FragmentMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Fragment 2"));
	FragmentMesh3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Fragment 3"));

	Fragments.Add(FragmentMesh1);
	Fragments.Add(FragmentMesh2);
	Fragments.Add(FragmentMesh3);

	for (UStaticMeshComponent* Fragment : Fragments) {
		Fragment->SetupAttachment(RootComponent);
	}

	KeyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Key Mesh"));

	StairwayToHeavenMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs1"));
	StairwayToHeavenMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs2"));
	StairwayToHeavenMesh3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs3"));
	StairwayToHeavenMesh4 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs4"));
	StairwayToHeavenMesh5 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs5"));
	StairwayToHeavenMesh6 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs6"));
	StairwayToHeavenMesh7 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs7"));
	StairwayToHeavenMesh8 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs8"));
	StairwayToHeavenMesh9 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs9"));
	StairwayToHeavenMesh10 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs10"));
	StairwayToHeavenMesh11 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs11"));
	StairwayToHeavenMesh12 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Stairs12"));

	Stairs.Add(StairwayToHeavenMesh1);
	Stairs.Add(StairwayToHeavenMesh2);
	Stairs.Add(StairwayToHeavenMesh3);
	Stairs.Add(StairwayToHeavenMesh4);
	Stairs.Add(StairwayToHeavenMesh5);
	Stairs.Add(StairwayToHeavenMesh6);
	Stairs.Add(StairwayToHeavenMesh7);
	Stairs.Add(StairwayToHeavenMesh8);
	Stairs.Add(StairwayToHeavenMesh9);
	Stairs.Add(StairwayToHeavenMesh10);
	Stairs.Add(StairwayToHeavenMesh11);
	Stairs.Add(StairwayToHeavenMesh12);

	for (UStaticMeshComponent* Stair : Stairs) {
		Stair->SetupAttachment(RootComponent);
	}

	SetReplicates(true);
}

void AHauntableReceptacle::BeginPlay() {
	Super::BeginPlay();

	BaseMesh->SetVisibility(true);
	BaseMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	FragmentMesh1->SetVisibility(true);
	FragmentMesh1->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	FragmentMesh2->SetVisibility(false);
	FragmentMesh2->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	FragmentMesh3->SetVisibility(false);
	FragmentMesh3->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	KeyMesh->SetVisibility(false);
	KeyMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	for (UStaticMeshComponent* Stair : Stairs) {
		Stair->SetVisibility(false);
		Stair->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	KeyBasePosition = KeyMesh->GetComponentTransform().GetLocation();
	KeyBaseRotation = KeyMesh->GetComponentTransform().GetRotation().Rotator();
	RotatingKeyAnimAxis.Normalize();

	
}

void AHauntableReceptacle::OnRep_FragmentNum() {
	for (int i = 0; i < FragmentNum; ++i) {
		Fragments[i]->SetVisibility(true);
		Fragments[i]->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	}
}

void AHauntableReceptacle::OnRep_Key() {
	if (bKey) {
		KeyMesh->SetVisibility(true);
		KeyMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	}
}

void AHauntableReceptacle::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	// Do the animation for the floating key
	if (bKey && FragmentNum >= 3) {
		FloatingKeyAnimTimeCount += DeltaTime;
		if (FloatingKeyAnimTimeCount > FloatingKeyAnimTime) {
			FloatingKeyAnimTimeCount -= FloatingKeyAnimTime;
		}
		// Compute the progress that should be between -1 and 1 (for sin)
		float Progress = (FloatingKeyAnimTimeCount / FloatingKeyAnimTime) * 2.0f*PI;
		FVector NewPos = KeyBasePosition + FloatingKeyAnimAxis * FMath::Sin(Progress);
		KeyMesh->SetWorldLocation(NewPos);
	}

	// Do the animation when spirit is doing its action
	if (bDoActionAnimation) {
		ApparitionAnimTimeCount += DeltaTime;
		// We stop the animation if the stairs is finished to be build
		if (ApparitionAnimTimeCount >= ApparitionAnimTime) {
			ApparitionAnimTimeCount = ApparitionAnimTime;
			bDoActionAnimation = false;
		}
		float Progress = ApparitionAnimTimeCount / ApparitionAnimTime;
		
		float RotSpeed = FMath::Sin(Progress * PI) * MaxRotatingSpeed;
		
		KeyMesh->AddRelativeRotation(FQuat(RotatingKeyAnimAxis * 359 * RotSpeed));
	}
}

bool AHauntableReceptacle::Place_Validate(APickableDoodad* Item) {
	return true;
}

void AHauntableReceptacle::Place_Implementation(APickableDoodad* Item) {
	// If this is a key
	if (Item->GetClass()->IsChildOf(APickableKey::StaticClass())) {
		bKey = true;
		OnRep_Key();
	} 
	// If this is a fragment
	else if (Item->GetClass()->IsChildOf(APickableFragment::StaticClass())) {
		FragmentNum++;
		OnRep_FragmentNum();
	}
	Item->Destroy();
}

void AHauntableReceptacle::Action_Implementation() {
	if (bKey && FragmentNum >= 3) {
		bActivated = true;
		OnRep_Activated();
	}
}

void AHauntableReceptacle::MakeAppearOneStair() {
	++LastAppearedStair;
	Stairs[LastAppearedStair]->SetVisibility(true);
	Stairs[LastAppearedStair]->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	if (LastAppearedStair >= Stairs.Num() - 1) {
		GetWorld()->GetTimerManager().ClearTimer(ApparitionTimerHandle);
	}
}

void AHauntableReceptacle::OnRep_Activated() {
	GetWorld()->GetTimerManager().SetTimer(ApparitionTimerHandle, this, &AHauntableReceptacle::MakeAppearOneStair, ApparitionAnimTime / Stairs.Num(), true);
	bDoActionAnimation = true;
}

void AHauntableReceptacle::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AHauntableReceptacle, FragmentNum);
	DOREPLIFETIME(AHauntableReceptacle, bKey);
	DOREPLIFETIME(AHauntableReceptacle, bActivated);
}