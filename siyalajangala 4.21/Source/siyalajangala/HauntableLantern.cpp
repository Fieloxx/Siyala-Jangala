// Fill out your copyright notice in the Description page of Project Settings.

#include "HauntableLantern.h"
#include "Utils.h"

AHauntableLantern::AHauntableLantern() : Super() {
	SetReplicates(true);
}

void AHauntableLantern::BeginPlay() {
	Super::BeginPlay();
	SetCorruption(ECorruptionState::CS_Weak);
}

void AHauntableLantern::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	// If we are server-side, control when the last purification was made.
	if (Role == ROLE_Authority && CorruptionState != ECorruptionState::CS_None) {
		if (bPurifySpirit || bPurifyJhada) {
			LastPurificationDelay += DeltaTime;

			if (LastPurificationDelay > MaxPurificationDelay) {
				// If we are beyond the maximum purification delay allowed
				// we set the purifications vars to false (the players were not 
				// enough in sync)
				bPurifyJhada = false;
				bPurifySpirit = false;
			}
		}
	}
}

bool AHauntableLantern::PurifySpirit_Validate() {
	return true;
}

void AHauntableLantern::PurifySpirit_Implementation() {
	bPurifySpirit = true;
	LastPurificationDelay = 0;

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Spirit purified"));
	}

	if (bPurifyJhada) {
		SetCorruption(ECorruptionState::CS_None);
	}
	else if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("   Jhada has not purified yet"));
	}
}

bool AHauntableLantern::PurifyJhada_Validate() {
	return true;
}

void AHauntableLantern::PurifyJhada_Implementation() {
	bPurifyJhada = true;
	LastPurificationDelay = 0;
	
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Jhada purified"));
	}

	if (bPurifySpirit) {
		SetCorruption(ECorruptionState::CS_None);
	}
	else if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("   Spirit has not purified yet"));
	}
}

void AHauntableLantern::SetCorruption_Implementation(ECorruptionState c_state) {
	Super::SetCorruption_Implementation(c_state);
	OnRep_CorruptionChanged();
}

void AHauntableLantern::OnRep_CorruptionChanged() {
	Super::OnRep_CorruptionChanged();

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Purify cosmetic"));
	}

	if (CorruptionState == ECorruptionState::CS_None) {
		Purify_Cosmetic();
		if (GEngine) {
			if (Role < ROLE_Authority) {
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("      CLIENT purify cosmetic"));
			}
			else {
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("      SERVER purify cosmetic"));
			}
		}
	}
	else {
		// TODO Make obvious the fact that the lantern is corrupted.
	}
}

void AHauntableLantern::Purify_Cosmetic_Implementation() {
	// Most of the implementation of this is done through the HauntableLantern blueprint
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Purify animation"));
	}
}

void AHauntableLantern::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}