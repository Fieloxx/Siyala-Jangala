#include "Bridge.h"
#include "Utils.h"
#include "HauntableDoodad.h"
#include "HauntablePlank.h"
#include "UnrealNetwork.h"

// Sets default values
ABridge::ABridge(): Super() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BridgeMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Bridge Mesh"));
	RootComponent = BridgeMesh;

	PlankMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plank Mesh 1"));
	PlankMesh1->SetupAttachment(RootComponent);
	PlankMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plank Mesh 2"));
	PlankMesh2->SetupAttachment(RootComponent);
	PlankMesh3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plank Mesh 3"));
	PlankMesh3->SetupAttachment(RootComponent);

	Block1 = CreateDefaultSubobject<UBoxComponent>(TEXT("BlockingBox1"));
	Block1->SetupAttachment(RootComponent);
	Block2 = CreateDefaultSubobject<UBoxComponent>(TEXT("BlockingBox2"));
	Block2->SetupAttachment(RootComponent);
	Block3 = CreateDefaultSubobject<UBoxComponent>(TEXT("BlockingBox3"));
	Block3->SetupAttachment(RootComponent);

	SetReplicates(true);
}

// Called when the game starts or when spawned
void ABridge::BeginPlay() {
	AActor::BeginPlay();

	BridgeMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BridgeMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);

	PlankMesh1->SetVisibility(false);
	PlankMesh1->SetSimulatePhysics(false);
	PlankMesh1->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlankMesh2->SetVisibility(false);
	PlankMesh2->SetSimulatePhysics(false);
	PlankMesh2->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlankMesh3->SetVisibility(false);
	PlankMesh3->SetSimulatePhysics(false);
	PlankMesh3->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	Block1->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Block1->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	Block1->SetEnableGravity(false);
	Block2->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Block2->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	Block2->SetEnableGravity(false);
	Block3->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Block3->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	Block3->SetEnableGravity(false);
}

// Called every frame
void ABridge::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

bool ABridge::PlacePlank_Validate(AHauntablePlank* Plank) {
	return true;
}

void ABridge::PlacePlank_Implementation(AHauntablePlank* Plank) {
	++Progress;
	OnRep_Progress();
	Plank->Destroy();
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("PlacePlank"));
	}
}

void ABridge::OnRep_Progress() {
	if (GEngine) {
		if (Role < ROLE_Authority) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("PlacePlank CLIENT"));
		}
		else {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("PlacePlank SERVER"));
		}
	}

	switch(Progress) {
		case 0: {
			break;
		}
		case 1: {
			PlankMesh1->SetVisibility(true);
			PlankMesh1->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
			Block1->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			break;
		}
		case 2: {
			PlankMesh2->SetVisibility(true);
			PlankMesh2->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
			Block2->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			break;
		}
		case 3: {
			PlankMesh3->SetVisibility(true);
			PlankMesh3->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
			Block3->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			break;
		}
		default: {
			if (GEngine) {
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Invalid progress in bridge, are you sure there isn't too many planks?"));
			}
			break;
		}
	}
	
}

void ABridge::GetLifetimeReplicatedProps(class TArray<class FLifetimeProperty, class FDefaultAllocator>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABridge, Progress);
}