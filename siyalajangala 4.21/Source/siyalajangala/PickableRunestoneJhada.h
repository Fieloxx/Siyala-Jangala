// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickableRunestone.h"
#include "PickableRunestoneJhada.generated.h"

/**
 * 
 */
UCLASS()
class SIYALAJANGALA_API APickableRunestoneJhada : public APickableRunestone
{
	GENERATED_BODY()
	
};
