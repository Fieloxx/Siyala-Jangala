// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Portal.generated.h"

class APickableDoodad;

UCLASS()
class SIYALAJANGALA_API APortal : public AActor {
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APortal();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* FrameMesh;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* SpiritRuneMesh;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* JhadaRuneMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* PortalMesh;

	bool isActivated;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(Server, Reliable, WithValidation)
	void PlaceRunestone(APickableDoodad* Runestone);
	
	UFUNCTION(NetMulticast, Reliable, WithValidation)
	virtual void MakeJhadaRuneAppear();
	UFUNCTION(NetMulticast, Reliable, WithValidation)
	virtual void MakeSpiritRuneAppear();
	UFUNCTION(NetMulticast, Reliable, WithValidation)
	virtual void ActivatePortalMesh();


	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;
};
