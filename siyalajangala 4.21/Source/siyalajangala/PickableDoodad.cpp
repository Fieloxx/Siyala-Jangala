#include "PickableDoodad.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "UnrealNetwork.h"
#include "Engine.h"
#include "Utils.h"

APickableDoodad::APickableDoodad() : AConcreteDoodad() {
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	DoodadMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoodadMesh"));
	DoodadMesh->SetSimulatePhysics(true);
	DoodadMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	RootComponent = Cast<USceneComponent>(DoodadMesh);
	
	SetReplicates(true);
	SetReplicateMovement(true);

	// Enhanced collision
	DoodadMesh->SetAllUseCCD(true);
}

void APickableDoodad::BeginPlay()
{
	Super::BeginPlay();

	SetHolding(false);
	SetGravity(true);

	// OLD // Replacement cause Jhada is not necessarely the first player character
	// Jhada = UGameplayStatics::GetPlayerCharacter(this, 0);

	// NEW
	if (Role == ROLE_Authority) {
		SetOwner(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	}

	TArray<AActor*> Jhadas;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AJhada::StaticClass(), Jhadas);
	if (Jhadas.Num() > 0) {
		// SetOwner(Jhadas[0]);
		SetJhada(Cast<ACharacter>(Jhadas[0]));
	}
	else {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Red, TEXT("Could not find Jhada !"));
			GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
		}
	}

	PlayerCamera = Jhada->FindComponentByClass<UCameraComponent>();

	TArray<USceneComponent*> Components;
	Jhada->GetComponents(Components);

	if (Components.Num() > 0) {
		for (auto& Comp : Components) {
			if (Comp->GetName() == "HoldingComponent") {
				HoldingComp = Cast<USceneComponent>(Comp);
				FRotator Rotation = HoldingComp->GetComponentRotation();
				Rotation.Yaw += 240;
				HoldingComp->SetRelativeRotation(Rotation);
			}
		}
	}

	// UNTESTED ON LEVEL1
	//Utils::SetOwnerToJhada(this);
	//SetOwner(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
}

// Called every frame
void APickableDoodad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*
	if (bHolding && HoldingComp) {
		SetActorLocationAndRotation(HoldingComp->GetComponentLocation() + PlayerCamera->GetForwardVector(), HoldingComp->GetComponentRotation());
	}
	*/
}

bool APickableDoodad::PickUp_Validate() {
	return true;
}

void APickableDoodad::PickUp_Implementation() {
	SetHolding(!bHolding);
	SetGravity(!bGravity);

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("APickableDoodad::PickUp()"));
	}

	// No need to multicast as doodadMesh is replicated
	// Object is released
	if (!bHolding) {
		Release();
	}
	
	// Object is taken
	else {
		Take();
	}
}

bool APickableDoodad::Take_Validate() {
	return true;
}

void APickableDoodad::Take_Implementation() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("\tItem TAKEN"));
	}
	DoodadMesh->SetEnableGravity(bGravity);
	DoodadMesh->SetSimulatePhysics(false);
	DoodadMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	AttachToActor(
		Jhada,
		FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false)
	);
	SetActorLocationAndRotation(HoldingComp->GetComponentLocation() + PlayerCamera->GetForwardVector(), HoldingComp->GetComponentRotation());
}

bool APickableDoodad::Release_Validate() {
	return true;
}

void APickableDoodad::Release_Implementation() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("\tItem RELEASED"));
	}
	DetachFromActor(FDetachmentTransformRules(EDetachmentRule::KeepWorld, EDetachmentRule::KeepWorld, EDetachmentRule::KeepWorld, false));

	DoodadMesh->SetEnableGravity(bGravity);
	DoodadMesh->SetSimulatePhysics(true);
	DoodadMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	ForwardVector = PlayerCamera->GetForwardVector();
	DoodadMesh->AddForce(ForwardVector * 5000 * DoodadMesh->GetMass());
}

bool APickableDoodad::SetHolding_Validate(bool NbHolding) {
	return true;
}

void APickableDoodad::SetHolding_Implementation(bool NbHolding) {
	bHolding = NbHolding;
}

bool APickableDoodad::SetGravity_Validate(bool NbGravity) {
	return true;
}

void APickableDoodad::SetGravity_Implementation(bool NbGravity) {
	bGravity = NbGravity;
}

void APickableDoodad::SetJhada(ACharacter* NJhada) {
	Jhada = NJhada;
	if (Role < ROLE_Authority) {
		SetJhada_Server(NJhada);
	}
}

bool APickableDoodad::SetJhada_Server_Validate(ACharacter* NJhada) {
	return true;
}

void APickableDoodad::SetJhada_Server_Implementation(ACharacter* NJhada) {
	Jhada = NJhada;
}

void APickableDoodad::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(APickableDoodad, Jhada);
	DOREPLIFETIME(APickableDoodad, DoodadMesh);
	DOREPLIFETIME(APickableDoodad, bHolding);
	DOREPLIFETIME(APickableDoodad, bGravity);
}