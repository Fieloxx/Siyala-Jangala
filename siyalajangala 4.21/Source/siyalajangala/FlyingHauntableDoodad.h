// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HauntableDoodad.h"
#include "GameFramework/Character.h"
#include "FlyingHauntableDoodad.generated.h"

UCLASS()
class SIYALAJANGALA_API AFlyingHauntableDoodad : public AHauntableDoodad
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFlyingHauntableDoodad();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// TODO voir ce qu'est Transient
	UPROPERTY(Transient, ReplicatedUsing = OnRep_PositionChanged)
	FVector NetworkPosition;

	bool bHasToMove;

	bool bPositionNeedUpdate;
	float LastPositionUpdateTime; // Last received position time
	const float MaxDistShift = 500.0f;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_RotationChanged)
	FRotator NetworkRotation;

	UPROPERTY(Transient, Replicated)
	FHitResult LastHitResult;

	bool bHasToRotate;
	FRotator RotationOffset;

	bool bRotationNeedUpdate;
	float LastRotationUpdateTime; // Last received rotation time
	const float MaxRotationShift = 45;

	const float SimulateMoveUpTime = 0.250f;
	float SimulateMoveUpTimeCount = 0.0f;
	float SimulateMoveUpAxis = 0.0f;
	/**
	* Speed in UE length unity per second
	**/
	float Speed;

	float MaxSpeedMoveUp = 2.0f;
	float MinSpeedMoveUp = 1.0f;
	float SmoothingSpeedMoveUp = 1.0f;
	/**
	* Max time in seconds where we can interpolate movement. 
	**/
	const float SmoothTransitionTime = 0.125f;

	UFUNCTION()
	void OnRep_PositionChanged();

	UFUNCTION()
	void OnRep_RotationChanged();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void MoveForward(float AxisValue);
	virtual void MoveRight(float AxisValue);
	virtual void MoveUp(float AxisValue);
	virtual void GoUp();
	virtual void GoDown();

	/**
	* Do the asked move setting NetworkPosition to the correct value
	* and LastHitResult, to the object that was hit.
	**/
	UFUNCTION(Server, Unreliable, WithValidation)
	virtual void Move(const FVector& MovementInput);

	virtual void RotateInput(float AxisValue);

	UFUNCTION(Server, Unreliable, WithValidation)
	virtual void Rotate(const FRotator& RotationInput);

	virtual void SmoothPositioning();
	virtual void SmoothRotationing();

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

	virtual bool IsRotationValid();
};
