#pragma once

#include "CoreMinimal.h"
#include "PickableDoodad.h"
#include "PickableKey.generated.h"


UCLASS()
class SIYALAJANGALA_API APickableKey : public APickableDoodad {
	GENERATED_BODY()
public:
	APickableKey();

protected:
	virtual void BeginPlay() override;
	FVector BaseLocation;

public:
	virtual void Tick(float DeltaTime) override;
	FVector GetBaseLocation() const;
};
