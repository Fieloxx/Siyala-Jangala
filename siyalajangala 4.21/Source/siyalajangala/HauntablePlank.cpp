#include "HauntablePlank.h"
#include "Bridge.h"
#include "SJ_PlayerController.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "UnrealNetwork.h"


AHauntablePlank::AHauntablePlank() : Super() {
	CurrentItem = nullptr;
	// SIMULATED MASS SHOULD BE SET TO 20 FOR ALL PLANKS IN EDITOR (NOT WORKING PROPERLY VIA C++)
}

void AHauntablePlank::BeginPlay() {
	Super::BeginPlay();
}

void AHauntablePlank::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (bPossessed) {
		//DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);

		Start = Camera->GetComponentLocation();
		ForwardVector = Camera->GetForwardVector();
		End = (ForwardVector * 2000.0f) + Start;

		if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
			if (Hit.GetActor()->GetClass()->IsChildOf(ABridge::StaticClass())) {
				SetCurrentItem(Cast<ABridge>(Hit.GetActor()));
			}
			else {
				if (Hit.GetActor() == this) {
					FVector NewStart = Hit.ImpactPoint + ForwardVector * (Hit.Distance + Hit.PenetrationDepth + 0.1);
					//DrawDebugLine(GetWorld(), NewStart, End, FColor::Purple, false, 1, 0, 1);
					GetWorld()->LineTraceSingleByChannel(Hit, NewStart, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams);
					if (Hit.IsValidBlockingHit() && Hit.GetActor()->GetClass()->IsChildOf(ABridge::StaticClass())) {
						SetCurrentItem(Cast<ABridge>(Hit.GetActor()));
					}
					else {
						SetCurrentItem(nullptr);
					}
				}
				else {
					SetCurrentItem(nullptr);
				}
			}
		}
		else {
			SetCurrentItem(nullptr);
		}
	}
}

void AHauntablePlank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AHauntablePlank::SetCurrentItem(ABridge* NCurrentItem) {
	// Verification to avoid overloading the server.
	if (CurrentItem != NCurrentItem) {
		SetCurrentItem_Server(NCurrentItem);
	}
}

bool AHauntablePlank::SetCurrentItem_Server_Validate(ABridge* CurrentItem) {
	return true;
}

void AHauntablePlank::SetCurrentItem_Server_Implementation(ABridge* NCurrentItem) {
	CurrentItem = NCurrentItem;
}


void AHauntablePlank::Action_Implementation() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Positionning plank."));
	}
	if (CurrentItem != nullptr) {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("    Found the bridge."));
		}
		Unpossess();
		CurrentItem->PlacePlank(this);
	}
}

void AHauntablePlank::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AHauntablePlank, CurrentItem);
}