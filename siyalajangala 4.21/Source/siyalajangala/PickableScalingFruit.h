// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickableDoodad.h"
#include "PickableScalingFruit.generated.h"

/**
 * 
 */
UCLASS()
class SIYALAJANGALA_API APickableScalingFruit : public APickableDoodad
{
	GENERATED_BODY()
	
public:
	APickableScalingFruit();

	virtual void Tick(float DeltaTime) override;

	virtual void Take_Implementation() override;
	virtual void Release_Implementation() override;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(Replicated)
	FVector Scale;

	float ScaleAmount = 3.0f;

	UFUNCTION(Server, Reliable, WithValidation)
	void SetScale(FVector NScale);

	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void SetJhadaScale(float ScaleFactor);

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;
};
