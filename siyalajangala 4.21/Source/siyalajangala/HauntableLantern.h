#pragma once

#include "CoreMinimal.h"
#include "HauntableMovableReal.h"
#include "HauntableLantern.generated.h"

UCLASS()
class SIYALAJANGALA_API AHauntableLantern : public AHauntableMovableReal {
	GENERATED_BODY()
	
public:
	AHauntableLantern();

protected:
	virtual void BeginPlay() override;


	/* Variables to keep track of who is purifying the lantern (and when).
	* This is only managed by the server itself.
	*/
	float LastPurificationDelay = 0;
	bool bPurifySpirit = false;
	bool bPurifyJhada = false;

	UPROPERTY(EditAnywhere)
	float MaxPurificationDelay = 20; // 20 for tests

public:
	virtual void Tick(float DeltaTime) override;

	/**
	* Called when spirit call purify on lantern, both (Jhada and spirit 
	* must call purify to be able to purify the lantern
	**/
	UFUNCTION(Server, Reliable, WithValidation)
	void PurifySpirit();
	/**
	* Called when Jhada call purify on lantern, both (Jhada and spirit)
	* must call purify to be able to purify the lantern.
	**/
	UFUNCTION(Server, Reliable, WithValidation)
	void PurifyJhada();

	virtual void SetCorruption_Implementation(ECorruptionState c_state) override;

	virtual void OnRep_CorruptionChanged() override;

	/**
	* Pure cosmetic function to show the purification change.
	**/
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, BlueprintCosmetic)
	void Purify_Cosmetic();

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;
};
