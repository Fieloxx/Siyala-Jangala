#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Brasero.generated.h"

class AHauntableLantern;

UCLASS()
class SIYALAJANGALA_API ABrasero : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABrasero();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* BraseroMesh;
	UPROPERTY(EditAnywhere)
	AHauntableLantern* Lantern = nullptr;
	UPROPERTY(EditAnywhere)
	UParticleSystemComponent* Particles;

	bool IsLanternNearby();

	UPROPERTY(EditAnywhere)
		float LanternDistance;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
