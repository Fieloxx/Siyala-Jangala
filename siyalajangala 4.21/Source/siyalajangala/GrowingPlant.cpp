#include "GrowingPlant.h"
#include "PickableDoodad.h"
#include "PickableSeed.h"
#include "HauntableDoodad.h"
#include "HauntableWater.h"
#include "PickableCristal.h"
#include "UnrealNetwork.h"
#include "Utils.h"

// Sets default values
AGrowingPlant::AGrowingPlant(): Super() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	EarthMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Earth Mesh"));
	//EarthMesh->SetupAttachment(RootComponent);
	RootComponent = EarthMesh;

	SeedMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Seed Mesh"));
	SeedMesh->SetupAttachment(RootComponent);
	
	WaterMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Water Mesh"));
	WaterMesh->SetupAttachment(RootComponent);
	
	PlantMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plant Mesh 1"));
	PlantMesh1->SetupAttachment(RootComponent);
	PlantMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plant Mesh 2"));
	PlantMesh2->SetupAttachment(RootComponent);
	PlantMesh3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plant Mesh 3"));
	PlantMesh3->SetupAttachment(RootComponent);
	PlantMesh4 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plant Mesh 4"));
	PlantMesh4->SetupAttachment(RootComponent);
	PlantMesh5 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plant Mesh 5"));
	PlantMesh5->SetupAttachment(RootComponent);
	
	Cristal = nullptr;

	bSeed = false;
	bWater = false;
	bGrown = false;

	SetReplicates(true);
}

// Called when the game starts or when spawned
void AGrowingPlant::BeginPlay() {
	Super::BeginPlay();
	
	EarthMesh->SetVisibility(true);
	SeedMesh->SetVisibility(false);
	SeedMesh->SetSimulatePhysics(false);
	SeedMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WaterMesh->SetVisibility(false);
	WaterMesh->SetSimulatePhysics(false);
	WaterMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlantMesh1->SetVisibility(false);
	PlantMesh1->SetSimulatePhysics(false);
	PlantMesh1->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlantMesh2->SetVisibility(false);
	PlantMesh2->SetSimulatePhysics(false);
	PlantMesh2->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlantMesh3->SetVisibility(false);
	PlantMesh3->SetSimulatePhysics(false);
	PlantMesh3->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlantMesh4->SetVisibility(false);
	PlantMesh4->SetSimulatePhysics(false);
	PlantMesh4->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PlantMesh5->SetVisibility(false);
	PlantMesh5->SetSimulatePhysics(false);
	PlantMesh5->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	if (Cristal != nullptr) {
		Cristal->SetActorHiddenInGame(true);
	}
}

void AGrowingPlant::OnRep_Seed() {
	// TODO Test
	if (bSeed && !bGrown) {
		// We control if the plant is not grown, if it is,
		// we only need replicate the grow event.
		if (GEngine) {
			if (Role < ROLE_Authority) {
				GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Green, TEXT("Seed replication CLIENT"));
			}
			else {
				GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Green, TEXT("Seed replication SERVER"));
			}
		}

		SeedMesh->SetVisibility(true);
	}
}

void AGrowingPlant::OnRep_Water() {
	// TODO Test
	if (bWater && !bGrown) {
		// We control if the plant is not grown, if it is,
		// we only need replicate the grow event.
		WaterMesh->SetVisibility(true);
	}
}

void AGrowingPlant::OnRep_Grown() {
	// TODO Test
	if (bGrown) {
		SeedMesh->SetVisibility(false);
		WaterMesh->SetVisibility(false);

		PlantMesh1->SetVisibility(true);
		PlantMesh1->SetSimulatePhysics(false);
		PlantMesh1->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		PlantMesh2->SetVisibility(true);
		PlantMesh2->SetSimulatePhysics(false);
		PlantMesh2->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		PlantMesh3->SetVisibility(true);
		PlantMesh3->SetSimulatePhysics(false);
		PlantMesh3->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		PlantMesh4->SetVisibility(true);
		PlantMesh4->SetSimulatePhysics(false);
		PlantMesh4->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		PlantMesh5->SetVisibility(true);
		PlantMesh5->SetSimulatePhysics(false);
		PlantMesh5->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		if (Cristal != nullptr) {
			Cristal->SetActorHiddenInGame(false);
		}
	}
}

// Called every frame
void AGrowingPlant::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

bool AGrowingPlant::PlaceSeed_Validate(APickableSeed* Seed) {
	return true;
}

void AGrowingPlant::PlaceSeed_Implementation(APickableSeed* Seed) {
	bSeed = true;
	if (bWater) {
		GrowPlant();
	}
	Seed->Destroy();
	OnRep_Seed();
}

bool AGrowingPlant::PlaceWater_Validate(AHauntableWater* Water) {
	return true;
}

void AGrowingPlant::PlaceWater_Implementation(AHauntableWater* Water) {
	bWater = true;
	if (bSeed) {
		GrowPlant();
	}
	Water->Destroy();
	OnRep_Water();
}

bool AGrowingPlant::GrowPlant_Validate() {
	return true;
}

void AGrowingPlant::GrowPlant_Implementation() {
	bGrown = true;
	OnRep_Grown();
}

bool AGrowingPlant::IsGrown() const {
	return bGrown;
}

bool AGrowingPlant::HasSeed() const {
	return bSeed;
}

bool AGrowingPlant::IsWatered() const {
	return bWater;
}

void AGrowingPlant::GetLifetimeReplicatedProps(class TArray<class FLifetimeProperty, class FDefaultAllocator>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AGrowingPlant, bSeed);
	DOREPLIFETIME(AGrowingPlant, bWater);
	DOREPLIFETIME(AGrowingPlant, bGrown);
}