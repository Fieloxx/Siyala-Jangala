#include "PickableKey.h"

APickableKey::APickableKey() : APickableDoodad() {}

void APickableKey::BeginPlay() {
	Super::BeginPlay();
	BaseLocation = GetTransform().GetLocation();
}

void APickableKey::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

FVector APickableKey::GetBaseLocation() const {
	return BaseLocation;
}

