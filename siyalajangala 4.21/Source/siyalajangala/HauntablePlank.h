#pragma once

#include "CoreMinimal.h"
#include "HauntableMovableReal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "HauntablePlank.generated.h"

class ABridge;

UCLASS()
class SIYALAJANGALA_API AHauntablePlank : public AHauntableMovableReal {
	GENERATED_BODY()

public:
	AHauntablePlank();

protected:
	virtual void BeginPlay() override;

	FVector Start;
	FVector ForwardVector;
	FVector End;
	FHitResult Hit;
	FComponentQueryParams DefaultComponentQueryParams;
	FCollisionResponseParams DefaultResponseParams;

	UPROPERTY(Replicated)
	ABridge* CurrentItem;

	UPROPERTY(EditAnywhere, Category = Gameplay)
		float minHeight;
	UPROPERTY(EditAnywhere, Category = Gameplay)
		float maxHeight;
	bool bHasToRotateY;

public:
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void SetCurrentItem(ABridge* NCurrentItem);
	UFUNCTION(Server, Reliable, WithValidation)
	virtual void SetCurrentItem_Server(ABridge* NCurrentItem);
	
	virtual void Action_Implementation() override;

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;
};
