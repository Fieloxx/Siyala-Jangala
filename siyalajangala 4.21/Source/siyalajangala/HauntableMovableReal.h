#pragma once

#include "CoreMinimal.h"
#include "FlyingHauntableDoodad.h"
#include "HauntableMovableReal.generated.h"

UCLASS()
class SIYALAJANGALA_API AHauntableMovableReal : public AFlyingHauntableDoodad
{
	GENERATED_BODY()

public:
	AHauntableMovableReal();

	virtual void BeginPlay() override;

	/*FVector2D MovementInput;
	float RotateInput;*/
	FRotator BaseRotation;
	FVector BasePosition;

	/** Simulated mass in kilograms, default value is 1.0kg
	**/
	UPROPERTY(EditAnywhere, Replicated)
	float SimulatedMass = 1.0f;

	UPROPERTY(ReplicatedUsing = OnRep_GravityChanged)
	bool bGravity;

	UPROPERTY(ReplicatedUsing = OnRep_PhysicsChanged)
	bool bPhysics;

	virtual void OnRep_PossessionChanged() override;

	UFUNCTION()
	virtual void OnRep_GravityChanged();

	UFUNCTION()
	virtual void OnRep_PhysicsChanged();

	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void Action_Implementation() override;

	virtual void Move_Implementation(const FVector& MovementInput) override;

	virtual void Unpossess_Implementation() override;


	virtual void ToggleGravity_Real();
	UFUNCTION(Server, Reliable, WithValidation)
	virtual void ToggleGravity(bool bEnabled);
	//UFUNCTION(NetMulticast, Reliable, WithValidation)
	//virtual void EnableGravity_Multicast(bool bEnabled);

	virtual void TogglePhysics_Real();
	UFUNCTION(Server, Reliable, WithValidation)
	virtual void TogglePhysics(bool bEnabled);
	//UFUNCTION(NetMulticast, Reliable, WithValidation)
	//virtual void TogglePhysics_Multicast(bool bEnabled);

	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;
};
