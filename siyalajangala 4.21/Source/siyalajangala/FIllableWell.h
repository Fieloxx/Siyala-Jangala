#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "FIllableWell.generated.h"

UCLASS()
class SIYALAJANGALA_API AFIllableWell : public AActor {
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFIllableWell();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* WellMesh;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* FilledWellMesh;
	UPROPERTY(EditAnywhere)
		class APickableKey* Key;

	UPROPERTY(Replicated)
		bool bFilling = false;
	UPROPERTY(ReplicatedUsing = OnRep_Filled)
		bool bFilled = false;
	UPROPERTY(EditAnywhere)
		float TimeToFill = 2.f;
	UPROPERTY(EditAnywhere)
		FVector MaxFillPosition;

	FVector OriginalFillPosition;
	float TimeSinceFillBeginning = 0.f;

	UFUNCTION()
		void OnRep_Filled();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Server, Reliable, WithValidation)
	void Fill(class AHauntableWater* Water);

	void GetLifetimeReplicatedProps(class TArray<class FLifetimeProperty, class FDefaultAllocator>& OutLifetimeProps) const override;
};
