// Fill out your copyright notice in the Description page of Project Settings.

#include "FlyingHauntableDoodad.h"

#include "DrawDebugHelpers.h"
#include "Engine/Engine.h"
#include "SJ_PlayerController.h"
#include "UnrealNetwork.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AFlyingHauntableDoodad::AFlyingHauntableDoodad(): Super()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bHasToMove = false;
	bHasToRotate = false;
	bReplicates = true;
	bReplicateMovement = false;
	Speed = 500.0f;
	bPositionNeedUpdate = false;
	bRotationNeedUpdate = false;

	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;
}

// Called when the game starts or when spawned
void AFlyingHauntableDoodad::BeginPlay()
{
	Super::BeginPlay();
}

void AFlyingHauntableDoodad::OnRep_PositionChanged() {
	bPositionNeedUpdate = true;
	float CurrentTime = GetGameTimeSinceCreation();
	LastPositionUpdateTime = CurrentTime;	
}

void AFlyingHauntableDoodad::OnRep_RotationChanged() {
	bRotationNeedUpdate = true;
	float CurrentTime = GetGameTimeSinceCreation();
	LastRotationUpdateTime = CurrentTime;
}

// Called every frame in Owner
void AFlyingHauntableDoodad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Move up simulation since it's not done through pressed keys but 
	// pressed events which are few compared to pressed event.
	if (SimulateMoveUpTimeCount < SimulateMoveUpTime) {
		SimulateMoveUpTimeCount += DeltaTime;
		if (SimulateMoveUpTimeCount > SimulateMoveUpTime) {
			SimulateMoveUpTimeCount = SimulateMoveUpTime;
		}

		MoveUp(SimulateMoveUpAxis);

		// Calculating SmoothingSpeedMoveUp to smooth the move
		float Progress = SimulateMoveUpTimeCount / SimulateMoveUpTime;
		SmoothingSpeedMoveUp = FMath::InterpEaseInOut(MinSpeedMoveUp, MaxSpeedMoveUp, Progress, 3);
	}

	// Network replication.
	SmoothRotationing();
	SmoothPositioning();

	// Move inputs
	if (bHasToMove) {
		FVector MovementInput = ConsumeMovementInputVector();
		MovementInput.Normalize();
		MovementInput.Z *= SmoothingSpeedMoveUp;
		MovementInput = MovementInput * Speed * DeltaTime;
		Move(MovementInput);
		bHasToMove = false;
	}

	// Rotation inputs
	if (bHasToRotate) {
		Rotate(RotationOffset);
		RotationOffset = FRotator::ZeroRotator;
		bHasToRotate = false;
	}

	// Debug vizualisation
	/*DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + GetActorForwardVector()*500.0f, FColor::Red);
	DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + GetActorRightVector()*500.0f, FColor::Purple);
	FMinimalViewInfo CameraInfo;
	Camera->GetEditorPreviewInfo(DeltaTime, CameraInfo);
	DrawDebugCamera(GetWorld(), CameraInfo.Location, CameraInfo.Rotation, CameraInfo.FOV, 5.0f, FColor::Green);*/
}

// Called to bind functionality to input
void AFlyingHauntableDoodad::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Possess", IE_Pressed, this, &AHauntableDoodad::Unpossess);
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AHauntableDoodad::Action);

	PlayerInputComponent->BindAxis("MoveForward", this, &AFlyingHauntableDoodad::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFlyingHauntableDoodad::MoveRight);
	PlayerInputComponent->BindAction("GoUp", IE_Pressed, this, &AFlyingHauntableDoodad::GoUp);
	PlayerInputComponent->BindAction("GoDown", IE_Pressed, this, &AFlyingHauntableDoodad::GoDown);

	PlayerInputComponent->BindAxis("Turn", this, &AFlyingHauntableDoodad::RotateInput);
}

void AFlyingHauntableDoodad::MoveForward(float AxisValue) {
	if (AxisValue != 0.0f) {
		AddMovementInput(GetActorForwardVector(), AxisValue, true);
		bHasToMove = true;
	}
}

void AFlyingHauntableDoodad::MoveRight(float AxisValue) {
	if (AxisValue != 0.0f) {
		AddMovementInput(GetActorRightVector(), AxisValue, true);
		bHasToMove = true;
	}
}

void AFlyingHauntableDoodad::MoveUp(float AxisValue) {
	if (AxisValue != 0.0f) {
		AddMovementInput(GetActorUpVector(), AxisValue, true);
		bHasToMove = true;
	}
}

void AFlyingHauntableDoodad::GoUp() {
	if (SimulateMoveUpAxis == -1.0f) {
		SimulateMoveUpAxis = 0.0f;
		SimulateMoveUpTimeCount = SimulateMoveUpTime;
	}
	else {
		SimulateMoveUpAxis = 1.0f;
		SimulateMoveUpTimeCount = 0.0f;
	}
}

void AFlyingHauntableDoodad::GoDown() {
	if (SimulateMoveUpAxis == 1.0f) {
		SimulateMoveUpAxis = 0.0f;
		SimulateMoveUpTimeCount = SimulateMoveUpTime;
	}
	else {
		SimulateMoveUpAxis = -1.0f;
		SimulateMoveUpTimeCount = 0.0f;
	}
}

bool AFlyingHauntableDoodad::Move_Validate(const FVector& MovementInput) {
	return true;
}

void AFlyingHauntableDoodad::Move_Implementation(const FVector& MovementInput) {
	AddActorWorldOffset(MovementInput, true, &LastHitResult);
	NetworkPosition = GetActorLocation();
}

void AFlyingHauntableDoodad::RotateInput(float AxisValue) {
	if (AxisValue != 0.0f) {
		RotationOffset.Yaw = AxisValue;
		bHasToRotate = true;
	}
}

bool AFlyingHauntableDoodad::Rotate_Validate(const FRotator& RotationInput) {
	return true;
}

void AFlyingHauntableDoodad::Rotate_Implementation(const FRotator& RotationInput) {
	AddActorWorldRotation(RotationInput);
	if(!IsRotationValid()) {
		AddActorWorldRotation(FRotator(0, 0, 0) - RotationInput);
		/*if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, "Resetting rotation");
		}*/

		// We need to check until which maximum rotation we can go.
		float RotationStepNum = FVector(RotationInput.Yaw, RotationInput.Pitch, RotationInput.Roll).Size();

		// DEBUG
		/*if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::FromInt(RotationStepNum));
		}*/

		// float RotationStepNum = FVector(RotationInput.Yaw, RotationInput.Pitch, RotationInput.Roll).Size()/(180.0/PI);
		FRotator ClosestRotationInput = FRotator(0, 0, 0);
		bool bIsStillValid = true;
		for (float i = 1; i <= RotationStepNum && bIsStillValid; ++i) {
			FRotator TempRotationInput = FMath::Lerp(FRotator(0, 0, 0), RotationInput, (i / RotationStepNum));
			AddActorWorldRotation(TempRotationInput);
			if (IsRotationValid()) {
				ClosestRotationInput = TempRotationInput;
			}
			else {
				bIsStillValid = false;
			}
			AddActorWorldRotation(FRotator(0, 0, 0) - TempRotationInput);
		}
		AddActorWorldRotation(ClosestRotationInput);
	}
	NetworkRotation = GetActorRotation();
}

void AFlyingHauntableDoodad::SmoothPositioning() {
	if (bPositionNeedUpdate) {
		float CurrentTime = GetGameTimeSinceCreation();
		float DelayWithLastUpdate = CurrentTime - LastPositionUpdateTime;
		if (DelayWithLastUpdate < SmoothTransitionTime) {
			float InterpolationFactor = FMath::FInterpTo(0.0f, 1.0f, DelayWithLastUpdate, 1.0f/SmoothTransitionTime);
			FVector NewPosition = GetActorLocation() * (1.0f - InterpolationFactor) + NetworkPosition * InterpolationFactor;
			SetActorLocation(NewPosition);
		}
		else {
			SetActorLocation(NetworkPosition);
			bPositionNeedUpdate = false;
		}
	}
}

void AFlyingHauntableDoodad::SmoothRotationing() {
	if (bRotationNeedUpdate) {
		float CurrentTime = GetGameTimeSinceCreation();
		float DelayWithLastUpdate = CurrentTime - LastRotationUpdateTime;
		if (DelayWithLastUpdate < SmoothTransitionTime) {
			FRotator InterpolationRotation = FMath::RInterpTo(GetActorRotation(), NetworkRotation, DelayWithLastUpdate, 1.0f/SmoothTransitionTime);
			SetActorRotation(InterpolationRotation);
		}
		else {
			// DEBUG
			/*if (GEngine) {
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, "Rotation is now synced.");
			}*/
			SetActorRotation(NetworkRotation);
			bRotationNeedUpdate = false;
		}
	}
	
}

void AFlyingHauntableDoodad::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AFlyingHauntableDoodad, NetworkPosition);
	DOREPLIFETIME(AFlyingHauntableDoodad, NetworkRotation);
	DOREPLIFETIME(AFlyingHauntableDoodad, LastHitResult);
}

bool AFlyingHauntableDoodad::IsRotationValid() {
	FHitResult* HitResult = new FHitResult();
	TArray<FVector> DirToTest = { 
		FVector(-1, 0, 0), FVector(1, 0, 0), 
		FVector(0, -1, 0), FVector(0, 1, 0), 
		FVector(0, 0, -1), FVector(0, 0, 1) 
	};
	unsigned int BlockedDir = 0;
	for (const FVector& Dir : DirToTest) {
		AddActorWorldOffset(Dir, true, HitResult);

		if (HitResult && HitResult->bBlockingHit) {
			BlockedDir++;
			if (BlockedDir >= 3) {
				delete HitResult;
				return false;
			}
		}
		else {
			AddActorWorldOffset(Dir*-1, false);
		}
	}
	delete HitResult;
	return true;
}


