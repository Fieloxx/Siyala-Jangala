#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Bridge.generated.h"

class AHauntableDoodad;
class AHauntablePlank;

UCLASS()
class SIYALAJANGALA_API ABridge : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABridge();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* BridgeMesh;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PlankMesh1;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PlankMesh2;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PlankMesh3;

	UPROPERTY(EditAnywhere)
	UBoxComponent* Block1;
	UPROPERTY(EditAnywhere)
	UBoxComponent* Block2;
	UPROPERTY(EditAnywhere)
	UBoxComponent* Block3;

	/**
	* represents the number of plank that have been brought to the bridge.
	**/
	UPROPERTY(ReplicatedUsing = OnRep_Progress)
	unsigned int Progress = 0;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void PlacePlank(AHauntablePlank* Plank);

	/**
	* Manages visual progression according to `Progress` variable.
	**/
	UFUNCTION()
	virtual void OnRep_Progress();

	virtual void GetLifetimeReplicatedProps(class TArray<class FLifetimeProperty, class FDefaultAllocator>& OutLifetimeProps) const override;
};
