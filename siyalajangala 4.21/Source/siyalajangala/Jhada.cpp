#include "Jhada.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "DrawDebugHelpers.h"
#include "PickableDoodad.h"
#include "PickableRunestone.h"
#include "Portal.h"
#include "GrowingPlant.h"
#include "PickableSeed.h"
#include "BriarBush.h"
#include "PickableCristal.h"
#include "SmellableObject.h"
#include "HauntableReceptacle.h"
#include "PickableFragment.h"
#include "PickableKey.h"
#include "GameFramework/PlayerController.h" // PlayerInputComponent
#include "GameFramework/CharacterMovementComponent.h"
#include "UnrealNetwork.h"
#include "HauntableLantern.h"
#include "Engine.h"
#include "Utils.h"

// Sets default values
AJhada::AJhada() : Super() {
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bAlwaysRelevant = true;

	JhadaCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	JhadaCameraComponent->SetupAttachment(GetCapsuleComponent());

	JhadaCameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 20 + BaseEyeHeight));
	JhadaCameraComponent->bUsePawnControlRotation = true;


	// Create a first person mesh component for the owning player.
	JhadaFPSMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonMesh"));
	// Only the owning player sees this mesh.
	JhadaFPSMesh->SetOnlyOwnerSee(true);
	// Attach the FPS mesh to the FPS camera.
	JhadaFPSMesh->SetupAttachment(JhadaCameraComponent);
	// Disable some environmental shadowing to preserve the illusion of having a single mesh.
	JhadaFPSMesh->bCastDynamicShadow = false;
	JhadaFPSMesh->CastShadow = false;

	SmellMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Smell Mesh"));
	SmellMesh->SetupAttachment(RootComponent);
	SmellMesh->SetOnlyOwnerSee(true);

	// The owning player doesn't see the regular (third-person) body mesh.
	GetMesh()->SetOwnerNoSee(true);

	HoldingComponent = CreateDefaultSubobject<USceneComponent>(TEXT("HoldingComponent"));
	HoldingComponent->SetupAttachment(GetCapsuleComponent());
	HoldingComponent->SetRelativeLocation(FVector(50.0f, 20.0f, 60.0f));

	CurrentItem = nullptr;
	Portal = nullptr;
	GrowingPlant = nullptr;
	BriarBush = nullptr;
	Receptacle = nullptr;
	Smellable = nullptr;

	SetReplicates(true);
	SetReplicateMovement(true);

	UE_LOG(LogTemp, Display, TEXT("Jhada now constructed"));

	//AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void AJhada::BeginPlay() {
	Super::BeginPlay();

	SmellMesh->SetVisibility(false);
	SmellMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SmellMesh->SetSimulatePhysics(false);

	Health = MaxHealth;

	if (Smellable) {
		Smellable->SetActorHiddenInGame(true);
	}
}

// Called every frame
void AJhada::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	
	Start = JhadaCameraComponent->GetComponentLocation();
	ForwardVector = JhadaCameraComponent->GetForwardVector();
	End = (ForwardVector * 500.0f) + Start;

	if (!MovementInput.IsZero()) {
		//Scale our movement input axis values by 300 units per second
		MovementInput = MovementInput.GetSafeNormal() * 500.0f;
		FVector NewLocation = GetActorLocation();
		NewLocation += GetActorForwardVector() * MovementInput.X * DeltaTime;
		NewLocation += GetActorRightVector() * MovementInput.Y * DeltaTime;
		SetActorLocation(NewLocation);
	}

	// DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);

	if (IsLocallyControlled()) {
		
		if (!HoldingItem) {
			if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECollisionChannel::ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
				if (Hit.GetActor()->GetClass()->IsChildOf(APickableDoodad::StaticClass())) {
					SetCurrentItem(Cast<APickableDoodad>(Hit.GetActor()));
					/*if (GEngine) {
						GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, TEXT("Gettin' " + Hit.GetActor()->GetName()));
					}*/
				}
				else {
					/*if (GEngine) {
						GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Yellow, TEXT("NOT gettin'" + Hit.GetActor()->GetName()));
					}*/
					SetCurrentItem(nullptr);
					SetPortal(nullptr);
				}

				if (Hit.GetActor()->GetClass()->IsChildOf(AHauntableLantern::StaticClass())) {
					SetLantern(Cast<AHauntableLantern>(Hit.GetActor()));
				}
				else {
					SetLantern(nullptr);
				}
			}
		}

		if (HoldingItem && HoldingItem->GetClass()->IsChildOf(APickableRunestone::StaticClass())) {
			if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
				// For line trace going through the runestone
				bool bHadCollisionEnabled = false;
				if (HoldingItem->GetActorEnableCollision()) {
					bHadCollisionEnabled = true;
					HoldingItem->SetActorEnableCollision(false);
				}

				if (Hit.GetActor()->GetClass()->IsChildOf(APortal::StaticClass())) {
					bPortal = true;
					SetPortal(Cast<APortal>(Hit.GetActor()));
				}
				else {
					bPortal = false;
					SetPortal(nullptr);
				}

				if (bHadCollisionEnabled) {
					HoldingItem->SetActorEnableCollision(true);
				}
			}
			else {
				bPortal = false;
				SetPortal(nullptr);
			}
		}

		if (HoldingItem && HoldingItem->GetClass()->IsChildOf(APickableSeed::StaticClass())) {
			// For line trace going through the seed
			bool bHadCollisionEnabled = false;
			if (HoldingItem->GetActorEnableCollision()) {
				bHadCollisionEnabled = true;
				HoldingItem->SetActorEnableCollision(false);
			}

			if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
				if (Hit.GetActor()->GetClass()->IsChildOf(AGrowingPlant::StaticClass())) {
					bGrowingPlant = true;
					SetGrowingPlant(Cast<AGrowingPlant>(Hit.GetActor()));
				}
				else {
					bGrowingPlant = false;
					SetGrowingPlant(nullptr);
				}
			}
			else {
				bGrowingPlant = false;
				SetGrowingPlant(nullptr);
			}

			if (bHadCollisionEnabled) {
				HoldingItem->SetActorEnableCollision(true);
			}
		}

		if (HoldingItem && HoldingItem->GetClass()->IsChildOf(APickableCristal::StaticClass())) {
			// For line trace going through the cristal
			bool bHadCollisionEnabled = false;
			if (HoldingItem->GetActorEnableCollision()) {
				bHadCollisionEnabled = true;
				HoldingItem->SetActorEnableCollision(false);
			}

			if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
				if (Hit.GetActor()->GetClass()->IsChildOf(ABriarBush::StaticClass())) {
					bBriarBush = true;
					SetBriarBush(Cast<ABriarBush>(Hit.GetActor()));
					//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("Briar bush in front!"));
				}
				else {
					bBriarBush = false;
					SetBriarBush(nullptr);
				}
			}
			else {
				bBriarBush = false;
				SetBriarBush(nullptr);
			}

			if (bHadCollisionEnabled) {
				HoldingItem->SetActorEnableCollision(true);
			}
		}

		if (HoldingItem && (HoldingItem->GetClass()->IsChildOf(APickableKey::StaticClass()) || HoldingItem->GetClass()->IsChildOf(APickableFragment::StaticClass()))) {
			// For line trace going through the key
			bool bHadCollisionEnabled = false;
			if (HoldingItem->GetActorEnableCollision()) {
				bHadCollisionEnabled = true;
				HoldingItem->SetActorEnableCollision(false);
			}

			if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECC_Visibility, DefaultComponentQueryParams, DefaultResponseParams)) {
				if (Hit.GetActor()->GetClass()->IsChildOf(AHauntableReceptacle::StaticClass())) {
					bReceptacle = true;
					SetReceptacle(Cast<AHauntableReceptacle>(Hit.GetActor()));
				}
				else {
					bReceptacle = false;
					SetReceptacle(nullptr);
				}
			}
			else {
				bReceptacle = false;
				SetReceptacle(nullptr);
			}

			if (bHadCollisionEnabled) {
				HoldingItem->SetActorEnableCollision(true);
			}
		}

		if (bPressedSmell && Smellable != nullptr) {
			if (!bHasSmelled) {
				SetHasSmelled(true);
			}
			DisplaySmell(true);
		}
		else {
			DisplaySmell(false);
		}

		if (!bHurting) {
			SafeLocation = GetActorLocation();
		}
	}
}

// Called to bind functionality to input
void AJhada::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	// Set up "movement" bindings.
	PlayerInputComponent->BindAxis("MoveForward", this, &AJhada::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AJhada::MoveRight);
	// Set up "look" bindings.
	PlayerInputComponent->BindAxis("Turn", this, &AJhada::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AJhada::AddControllerPitchInput);

	// Set up "action" bindings.
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AJhada::StartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AJhada::StopJump);

	PlayerInputComponent->BindAction("Smell", IE_Pressed, this, &AJhada::StartSmelling);
	PlayerInputComponent->BindAction("Smell", IE_Released, this, &AJhada::StopSmelling);

	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AJhada::ToggleItemPickup);

	PlayerInputComponent->BindAction("Purify", IE_Pressed, this, &AJhada::Purify);

}

// [NETWORK]
float AJhada::TakeDamage(float DamageAmount, const FDamageEvent& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Took damage"));
	}

	bHurting = true;
	Health -= DamageAmount;

	// Make the screen purple (in blueprints)

	if (Health <= 0) {
		// Teleport on the outside of the zone
		SetActorLocation(SafeLocation);
		bHurting = false;
		Health = MaxHealth;
	}

	return 0;
}


void AJhada::MoveForward(float AxisValue) {
	AddMovementInput(AxisValue*GetActorForwardVector());
	//MovementInput.X = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void AJhada::MoveRight(float AxisValue) {
	AddMovementInput(AxisValue*GetActorRightVector());
	//MovementInput.Y = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void AJhada::StartJump() {
	bPressedJump = true;
}

void AJhada::StopJump() {
	bPressedJump = false;
}


void AJhada::StartSmelling() {
	bPressedSmell = true;
}

void AJhada::StopSmelling() {
	bPressedSmell = false;
}

bool AJhada::Purify_Validate() {
	return true;
}

void AJhada::Purify_Implementation() {
	if (Lantern) {
		Lantern->PurifyJhada();
	}
}

void AJhada::ToggleItemPickup() {
	ToggleItemPickup_Server();
}

bool AJhada::ToggleItemPickup_Server_Validate() {
	return true;
}

void AJhada::ToggleItemPickup_Server_Implementation() {	
	if (HoldingItem) {
		if (Portal) { // TEST
			Portal->PlaceRunestone(HoldingItem);
			HoldingItem = nullptr;
			bPortal = false;
			CurrentItem = nullptr;
			Portal = nullptr;
		}
		else if (GrowingPlant) {
			UE_LOG(LogTemp, Warning, TEXT("Placing seed"));
			GrowingPlant->PlaceSeed(Cast<APickableSeed>(HoldingItem));
			HoldingItem = nullptr;
			bGrowingPlant = false;
			CurrentItem = nullptr;
			GrowingPlant = nullptr;
			if (GEngine) {
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Cyan, TEXT("Growing plant ERASED !!!!"));
			}
		}
		else if (BriarBush) { // TEST
			UE_LOG(LogTemp, Warning, TEXT("Moving bush"));
			BriarBush->MoveBush();
			BriarBush = nullptr;
		}
		else if (Receptacle) {
			Receptacle->Place(HoldingItem);
			HoldingItem = nullptr;
			bReceptacle = false;
			CurrentItem = nullptr;
			Receptacle = nullptr;
		}
		else {
			// This is a "normal" pickable doodad we have to release
			LeaveItem();
		}
	}
	else if (CurrentItem) {
		TakeItem();
	}
	else {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Cyan, TEXT("No item to pick."));
		}
	}
}

void AJhada::TakeItem() {
	HoldingItem = CurrentItem;
	HoldingItem->PickUp();
	CurrentItem = nullptr;
}

void AJhada::LeaveItem() {
	HoldingItem->PickUp();
	HoldingItem = nullptr;
}

bool AJhada::SetCurrentItem_Validate(class APickableDoodad* NCurrentItem) {
	return true;
}

void AJhada::SetCurrentItem_Implementation(class APickableDoodad* NCurrentItem) {
	CurrentItem = NCurrentItem;
}

bool AJhada::SetPortal_Validate(class APortal* NPortal) {
	return true;
}

void AJhada::SetPortal_Implementation(class APortal* NPortal) {
	Portal = NPortal;
}

bool AJhada::SetGrowingPlant_Validate(class AGrowingPlant* NGrowingPlant) {
	return true;
}

void AJhada::SetGrowingPlant_Implementation(class AGrowingPlant* NGrowingPlant) {
	if (GEngine) {
		if (NGrowingPlant) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Cyan, TEXT("Growing plant found"));
		}
		else {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Cyan, TEXT("Growing plant NOT found"));
		}
	}
	
	if (NGrowingPlant && !NGrowingPlant->HasSeed()) {
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Cyan, TEXT("Growing plant found !!!!"));
		}
		GrowingPlant = NGrowingPlant;
	}
}

bool AJhada::SetBriarBush_Validate(class ABriarBush* NBriarBush) {
	return true;
}

void AJhada::SetBriarBush_Implementation(class ABriarBush* NBriarBush) {
	BriarBush = NBriarBush;
}

bool AJhada::SetReceptacle_Validate(class AHauntableReceptacle* NReceptacle) {
	return true;
}

void AJhada::SetReceptacle_Implementation(class AHauntableReceptacle* NReceptacle) {
	Receptacle = NReceptacle;
}

bool AJhada::SetLantern_Validate(class AHauntableLantern* NLantern) {
	return true;
}

void AJhada::SetLantern_Implementation(class AHauntableLantern* NLantern) {
	Lantern = NLantern;
}

bool AJhada::SetHasSmelled_Validate(bool NbHasSmelled) {
	return true;
}

void AJhada::SetHasSmelled_Implementation(bool NbHasSmelled) {
	bHasSmelled = NbHasSmelled;
	OnRep_HasSmelled();
}

void AJhada::OnRep_HasSmelled() {
	if (bHasSmelled && Smellable != nullptr) {
		Smellable->SetActorHiddenInGame(false);
	}
}

void AJhada::OnRep_HoldingItem() {

}

void AJhada::OnRep_CurrentItem() {

}


void AJhada::DisplaySmell(bool bDisplayed) {
	if (bDisplayed) {
		SmellMesh->SetVisibility(true);

		ISmellableObject* SmellTest = Cast<ISmellableObject>(Smellable);

		if (SmellTest) {

			FVector StartLocation = this->GetActorLocation();
			FVector EndLocation = Smellable->GetActorLocation();

			FVector Vector = EndLocation - StartLocation;
			Vector = Vector.GetSafeNormal() * 300;

			SmellMesh->SetWorldLocation(StartLocation + Vector);
		}
		else {
			if (GEngine) {
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Not smelling anything !"));
			}
		}
	}
	else {
		SmellMesh->SetVisibility(false);
	}
}

void AJhada::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AJhada, HoldingComponent);
	DOREPLIFETIME(AJhada, HoldingItem);
	DOREPLIFETIME(AJhada, bPortal);
	DOREPLIFETIME(AJhada, bGrowingPlant);
	DOREPLIFETIME(AJhada, bBriarBush);
	DOREPLIFETIME(AJhada, bReceptacle);
	DOREPLIFETIME(AJhada, CurrentItem);
	DOREPLIFETIME(AJhada, Portal);
	DOREPLIFETIME(AJhada, GrowingPlant);
	DOREPLIFETIME(AJhada, BriarBush);
	DOREPLIFETIME(AJhada, Receptacle);
	DOREPLIFETIME(AJhada, Lantern);
	DOREPLIFETIME(AJhada, bHasSmelled);
}