#include "Brasero.h"
#include "HauntableLantern.h"
#include "Engine.h"

#include <string>

// Sets default values
ABrasero::ABrasero() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BraseroMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Brasero Mesh"));
	RootComponent = BraseroMesh;

	Particles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particles"));
	Particles->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABrasero::BeginPlay() {
	AActor::BeginPlay();
	Particles->Deactivate();
	Particles->InitializeSystem();
	Particles->InitParticles();

	// If user does not set the lantern, take the 1st lantern we meet.
	if (!Lantern) {
		TArray<AActor*> Lanterns;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AHauntableLantern::StaticClass(), Lanterns);
		if (Lanterns.Num() > 0) {
			Lantern = Cast<AHauntableLantern>(Lanterns[0]);
		}
		else {
			Lantern = nullptr;
		}
	}
}

// Called every frame
void ABrasero::Tick(float DeltaTime) {
	AActor::Tick(DeltaTime);

	if (!Particles->bHasBeenActivated && IsLanternNearby()) {
		Particles->Activate();
	}
	else {
	}
}


bool ABrasero::IsLanternNearby() {
	if (Lantern) {
		float Distance = FVector::Distance(this->GetActorLocation(), Lantern->GetActorLocation());
		return Distance < LanternDistance;
	}
	else {
		return false;
	}
}
