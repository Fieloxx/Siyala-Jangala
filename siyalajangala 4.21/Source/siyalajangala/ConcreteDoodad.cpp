// Fill out your copyright notice in the Description page of Project Settings.


#include "ConcreteDoodad.h"
#include "UnrealNetwork.h"

// Sets default values
AConcreteDoodad::AConcreteDoodad() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CorruptionState = ECorruptionState::CS_None;
	
	// Both lines below are necessary to see movement replication
	bReplicates = true;
	bReplicateMovement = true;
}

// Called when the game starts or when spawned
void AConcreteDoodad::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AConcreteDoodad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

ECorruptionState AConcreteDoodad::GetCorruption() const {
	return CorruptionState;
}

bool AConcreteDoodad::SetCorruption_Validate(ECorruptionState c_state) {
	return true;
}

void AConcreteDoodad::SetCorruption_Implementation(ECorruptionState c_state) {
	CorruptionState = c_state;
}

void AConcreteDoodad::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AConcreteDoodad, CorruptionState);
}
