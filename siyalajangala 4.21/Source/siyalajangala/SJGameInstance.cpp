#include "SJGameInstance.h"

#include "Online.h"
#include "Engine.h"
#include "MoviePlayer.h"
#include "CustomGameViewportClient.h"

USJGameInstance::USJGameInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	/** Bind function for CREATING a Session */
	OnCreateSessionCompleteDelegate = FOnCreateSessionCompleteDelegate::CreateUObject(this, &USJGameInstance::OnCreateSessionComplete);
	OnStartSessionCompleteDelegate = FOnStartSessionCompleteDelegate::CreateUObject(this, &USJGameInstance::OnStartOnlineGameComplete);
	/** Bind function for FINDING a Session */
	OnFindSessionsCompleteDelegate = FOnFindSessionsCompleteDelegate::CreateUObject(this, &USJGameInstance::OnFindSessionsComplete);
	/** Bind function for JOINING a Session */
	OnJoinSessionCompleteDelegate = FOnJoinSessionCompleteDelegate::CreateUObject(this, &USJGameInstance::OnJoinSessionComplete);
	/** Bind function for DESTROYING a Session */
	OnDestroySessionCompleteDelegate = FOnDestroySessionCompleteDelegate::CreateUObject(this, &USJGameInstance::OnDestroySessionComplete);
}

FString USJGameInstance::GetPlayerTypeAsString(SJEPlayerType EnumValue)
{
	const UEnum* EnumPtr = FindObject<UEnum>(nullptr, TEXT("SJEPlayerType"), true);
	if (!EnumPtr) return FString("Invalid");

	return EnumPtr->GetNameByValue((int64)EnumValue).ToString(); // for EnumValue == VE_Dance returns "VE_Dance"
}

SJEPlayerType USJGameInstance::GetPlayerTypeFromString(const FString& EnumName, const FString& String)
{
	UEnum* Enum = FindObject<UEnum>(nullptr, *EnumName, true);
	if (!Enum)
	{
		return SJEPlayerType(0);
	}
	return (SJEPlayerType)Enum->GetValueByName(FName(*String));
}


bool USJGameInstance::HostSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers)
{
	// Get the Online Subsystem to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		// Get the Session Interface, so we can call the "CreateSession" function on it
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid() && UserId.IsValid())
		{
			/*
				Fill in all the Session Settings that we want to use.

				There are more with SessionSettings.Set(...);
				For example the Map or the GameMode/Type.
			*/

			SessionSettings = MakeShareable(new FOnlineSessionSettings());
			
			SessionSettings->bIsLANMatch = bIsLAN;
			SessionSettings->bUsesPresence = bIsPresence;
			SessionSettings->NumPublicConnections = MaxNumPlayers;
			SessionSettings->NumPrivateConnections = 0;
			SessionSettings->bAllowInvites = true;
			SessionSettings->bAllowJoinInProgress = true;
			SessionSettings->bShouldAdvertise = true;
			SessionSettings->bAllowJoinViaPresence = true;
			SessionSettings->bAllowJoinViaPresenceFriendsOnly = false;

			// Configuring player type
			SessionSettings->Set("PlayerType", Utils::GetEnumValueAsString<SJEPlayerType>("SJEPlayerType", AvailablePlayerType), EOnlineDataAdvertisementType::ViaOnlineService);
			// -- Now available player type will be the pawn played by the host himself
			switch (AvailablePlayerType) {
			case SJEPlayerType::Jhada:
				AvailablePlayerType = SJEPlayerType::Spirit;
				break;
			case SJEPlayerType::Spirit:
				AvailablePlayerType = SJEPlayerType::Jhada;
				break;
			}

			SessionSettings->Set("CustomSessionName", CustomSessionName, EOnlineDataAdvertisementType::ViaOnlineService);

			SessionSettings->Set(SETTING_MAPNAME, FString("Default_Level"), EOnlineDataAdvertisementType::ViaOnlineService);

			// Set the delegate to the Handle of the SessionInterface
			OnCreateSessionCompleteDelegateHandle = Sessions->AddOnCreateSessionCompleteDelegate_Handle(OnCreateSessionCompleteDelegate);

			// Our delegate should get called when this is complete (doesn't need to be successful!)
			return Sessions->CreateSession(*UserId, SessionName, *SessionSettings);
		}
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("No OnlineSubsytem found!"));
	}

	return false;
}

void USJGameInstance::OnCreateSessionComplete(FName SessionName, bool bWasSuccessful)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OnCreateSessionComplete %s, %d"), *SessionName.ToString(), bWasSuccessful));

	// Get the OnlineSubsystem so we can get the Session Interface
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get the Session Interface to call the StartSession function
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			// Clear the SessionComplete delegate handle, since we finished this call
			Sessions->ClearOnCreateSessionCompleteDelegate_Handle(OnCreateSessionCompleteDelegateHandle);
			if (bWasSuccessful)
			{
				// Set the StartSession delegate handle
				OnStartSessionCompleteDelegateHandle = Sessions->AddOnStartSessionCompleteDelegate_Handle(OnStartSessionCompleteDelegate);

				// Our StartSessionComplete delegate should get called after this
				Sessions->StartSession(SessionName);
				
			}
		}
	}
	OnStartSessionCompleted(bWasSuccessful);
}

void USJGameInstance::OnStartOnlineGameComplete(FName SessionName, bool bWasSuccessful)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OnStartSessionComplete %s, %d"), *SessionName.ToString(), bWasSuccessful));

	// Get the Online Subsystem so we can get the Session Interface
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get the Session Interface to clear the Delegate
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid())
		{
			// Clear the delegate, since we are done with this call
			Sessions->ClearOnStartSessionCompleteDelegate_Handle(OnStartSessionCompleteDelegateHandle);
		}
	}

	// If the start was successful, we can open a NewMap if we want. Make sure to use "listen" as a parameter!
	if (bWasSuccessful)
	{
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Yellow, ("Trying to open \""+ChosenLevel.ToString()+"\"."));
		}
		UGameplayStatics::OpenLevel(GetWorld(), ChosenLevel, true, "listen");
	}
}

void USJGameInstance::FindSessions(TSharedPtr<const FUniqueNetId> UserId, bool bIsLAN, bool bIsPresence)
{
	// Get the OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		// Get the SessionInterface from our OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid() && UserId.IsValid())
		{
			/*
				Fill in all the SearchSettings, like if we are searching for a LAN game and how many results we want to have!
			*/
			SessionSearch = MakeShareable(new FOnlineSessionSearch());

			SessionSearch->bIsLanQuery = bIsLAN;
			SessionSearch->MaxSearchResults = 20;
			SessionSearch->PingBucketSize = 50;

			// We only want to set this Query Setting if "bIsPresence" is true
			if (bIsPresence)
			{
				SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, bIsPresence, EOnlineComparisonOp::Equals);
			}

			TSharedRef<FOnlineSessionSearch> SearchSettingsRef = SessionSearch.ToSharedRef();

			// Set the Delegate to the Delegate Handle of the FindSession function
			OnFindSessionsCompleteDelegateHandle = Sessions->AddOnFindSessionsCompleteDelegate_Handle(OnFindSessionsCompleteDelegate);

			// Finally call the SessionInterface function. The Delegate gets called once this is finished
			Sessions->FindSessions(*UserId, SearchSettingsRef);
		}
	}
	else
	{
		// If something goes wrong, just call the Delegate Function directly with "false".
		OnFindSessionsComplete(false);
	}
}

void USJGameInstance::OnFindSessionsComplete(bool bWasSuccessful)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OFindSessionsComplete bSuccess: %d"), bWasSuccessful));

	// Get OnlineSubsystem we want to work with
	IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get SessionInterface of the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();
		if (Sessions.IsValid())
		{
			// Clear the Delegate handle, since we finished this call
			Sessions->ClearOnFindSessionsCompleteDelegate_Handle(OnFindSessionsCompleteDelegateHandle);

			// Just debugging the Number of Search results. Can be displayed in UMG or something later on
			GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Num Search Results: %d"), SessionSearch->SearchResults.Num()));

			// If we have found at least 1 session, we just going to debug them. You could add them to a list of UMG Widgets, like it is done in the BP version!
			if (SessionSearch->SearchResults.Num() > 0)
			{
				// "SessionSearch->SearchResults" is an Array that contains all the information. You can access the Session in this and get a lot of information.
				// This can be customized later on with your own classes to add more information that can be set and displayed
				for (int32 SearchIdx = 0; SearchIdx < SessionSearch->SearchResults.Num(); SearchIdx++)
				{
					// OwningUserName is just the SessionName for now. I guess you can create your own Host Settings class and GameSession Class and add a proper GameServer Name here.
					// This is something you can't do in Blueprint for example!
					GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("Session Number: %d | Sessionname: %s "), SearchIdx + 1, *(SessionSearch->SearchResults[SearchIdx].Session.OwningUserName)));
				}
			}
		}
	}
	OnFindSessionsCompleted(bWasSuccessful);
}

bool USJGameInstance::JoinSession(ULocalPlayer * LocalPlayer, int32 SessionIndexInSearchResults) {
	// Return bool
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Green, "Je suis l� !");
	bool bSuccessful = false;

	// Get UserID
	const TSharedPtr<const FUniqueNetId> UserId = LocalPlayer->GetPreferredUniqueNetId().GetUniqueNetId();

	// Get OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		// Get SessionInterface from the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid() && UserId.IsValid())
		{
			// Set the Handle again
			OnJoinSessionCompleteDelegateHandle = Sessions->AddOnJoinSessionCompleteDelegate_Handle(OnJoinSessionCompleteDelegate);

			// Setting available player type accordingly
			FString PlayerTypeStr;
			SessionSearch->SearchResults[SessionIndexInSearchResults].Session.SessionSettings.Get(FName("PlayerType"), PlayerTypeStr);
			AvailablePlayerType = Utils::GetEnumValueFromString<SJEPlayerType>(
				"SJEPlayerType",
				PlayerTypeStr
				);

			// Call the "JoinSession" Function with the passed "SearchResult". The "SessionSearch->SearchResults" can be used to get such a
			// "FOnlineSessionSearchResult" and pass it. Pretty straight forward!
			bSuccessful = Sessions->JoinSession(*UserId, EName::NAME_GameSession, SessionSearch->SearchResults[SessionIndexInSearchResults]);
		}
	}

	return bSuccessful;
}

bool USJGameInstance::JoinOnlineGame(FSJSessionResult Result) {
	// Return bool
	bool bSuccessful = false;

	// Get UserID
	ULocalPlayer* const LocalPlayer = GetFirstGamePlayer();
	return JoinSession(LocalPlayer, Result.SessionIndexInSearchResults);
}
/*
bool USJGameInstance::JoinSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult)
{
	// Return bool
	bool bSuccessful = false;

	// Get OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();

	if (OnlineSub)
	{
		// Get SessionInterface from the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid() && UserId.IsValid())
		{
			// Set the Handle again
			OnJoinSessionCompleteDelegateHandle = Sessions->AddOnJoinSessionCompleteDelegate_Handle(OnJoinSessionCompleteDelegate);

			// Call the "JoinSession" Function with the passed "SearchResult". The "SessionSearch->SearchResults" can be used to get such a
			// "FOnlineSessionSearchResult" and pass it. Pretty straight forward!
			bSuccessful = Sessions->JoinSession(*UserId, SessionName, SearchResult);
		}
	}

	return bSuccessful;
}*/

void USJGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Green, "PlayerTypeFound=" + Utils::GetEnumValueAsString<SJEPlayerType>("SJEPlayerType", AvailablePlayerType));
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OnJoinSessionComplete %s, %d"), *SessionName.ToString(), static_cast<int32>(Result)));

	// Get the OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get SessionInterface from the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			// Clear the Delegate again
			Sessions->ClearOnJoinSessionCompleteDelegate_Handle(OnJoinSessionCompleteDelegateHandle);

			// Get the first local PlayerController, so we can call "ClientTravel" to get to the Server Map
			// This is something the Blueprint Node "Join Session" does automatically!
			APlayerController * const PlayerController = GetFirstLocalPlayerController();

			// We need a FString to use ClientTravel and we can let the SessionInterface contruct such a
			// String for us by giving him the SessionName and an empty String. We want to do this, because
			// Every OnlineSubsystem uses different TravelURLs
			FString TravelURL;

			if (PlayerController && Sessions->GetResolvedConnectString(SessionName, TravelURL))
			{
				// Finally call the ClienTravel. If you want, you could print the TravelURL to see
				// how it really looks like
				PlayerController->ClientTravel(TravelURL, ETravelType::TRAVEL_Absolute);
				GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString("All good now"));
			}
		}
	}

	switch (Result) {
		case EOnJoinSessionCompleteResult::Type::Success:
			OnJoinSessionCompleted(true);
			LastErrorReason = "";
			break;
		case EOnJoinSessionCompleteResult::Type::SessionIsFull:
			OnJoinSessionCompleted(false);
			LastErrorReason = "Session is full";
			break;
		case EOnJoinSessionCompleteResult::Type::SessionDoesNotExist:
			OnJoinSessionCompleted(false);
			LastErrorReason = "Session does not exist";
			break;
		case EOnJoinSessionCompleteResult::Type::CouldNotRetrieveAddress:
			OnJoinSessionCompleted(false);
			LastErrorReason = "Could not retrieve address";
			break;
		case EOnJoinSessionCompleteResult::Type::AlreadyInSession:
			OnJoinSessionCompleted(false);
			LastErrorReason = "Already in session";
			break;
		case EOnJoinSessionCompleteResult::Type::UnknownError:
			OnJoinSessionCompleted(false);
			LastErrorReason = "Unknown error";
			break;
	}
}

void USJGameInstance::OnDestroySessionComplete(FName SessionName, bool bWasSuccessful)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OnDestroySessionComplete %s, %d"), *SessionName.ToString(), bWasSuccessful));

	// Get the OnlineSubsystem we want to work with
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		// Get the SessionInterface from the OnlineSubsystem
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			// Clear the Delegate
			Sessions->ClearOnDestroySessionCompleteDelegate_Handle(OnDestroySessionCompleteDelegateHandle);

			// If it was successful, we just load another level (could be a MainMenu!)
			if (bWasSuccessful)
			{
				UGameplayStatics::OpenLevel(GetWorld(), "ThirdPersonExampleMap", true);
			}
		}
	}
}

void USJGameInstance::Init() {
	Super::Init();

	FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &USJGameInstance::BeginLoadingScreen);
	FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &USJGameInstance::EndLoadingScreen);
}

void USJGameInstance::StartOnlineGame(bool bUseLan)
{
	// Creating a local player where we can get the UserID from
	ULocalPlayer* const Player = GetFirstGamePlayer();

	// Call our custom HostSession function. GameSessionName is a GameInstance variable
	HostSession(Player->GetPreferredUniqueNetId().GetUniqueNetId(), GameSessionName, bUseLan, true, 2);
}

void USJGameInstance::FindOnlineGames(bool bUseLan)
{
	ULocalPlayer* const Player = GetFirstGamePlayer();

	FindSessions(Player->GetPreferredUniqueNetId().GetUniqueNetId(), bUseLan, true);
}

/*
void USJGameInstance::JoinOnlineGame()
{
	ULocalPlayer* const Player = GetFirstGamePlayer();

	// Just a SearchResult where we can save the one we want to use, for the case we find more than one!
	FOnlineSessionSearchResult SearchResult;

	// If the Array is not empty, we can go through it
	if (SessionSearch->SearchResults.Num() > 0)
	{
		for (int32 i = 0; i < SessionSearch->SearchResults.Num(); i++)
		{
			// To avoid something crazy, we filter sessions from ourself
			if (SessionSearch->SearchResults[i].Session.OwningUserId != Player->GetPreferredUniqueNetId())
			{
				SearchResult = SessionSearch->SearchResults[i];

				// Once we found sounce a Session that is not ours, just join it. Instead of using a for loop, you could
				// use a widget where you click on and have a reference for the GameSession it represents which you can use
				// here
				JoinSession(Player->GetPreferredUniqueNetId().GetUniqueNetId, GameSessionName, SessionIndexInSearchResults);
				break;
			}
		}
	}
}*/

void USJGameInstance::DestroySessionAndLeaveGame()
{
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr Sessions = OnlineSub->GetSessionInterface();

		if (Sessions.IsValid())
		{
			Sessions->AddOnDestroySessionCompleteDelegate_Handle(OnDestroySessionCompleteDelegate);

			Sessions->DestroySession(GameSessionName);
		}
	}
}
/*
void USJGameInstance::GetPlayableCharacter(FBlueprintSessionResult result, FString& PlayableCharacter) 
{
	result.OnlineResult.Session.SessionSettings.Get("PlayerType", PlayableCharacter);
}*/

void USJGameInstance::GetSearchResults(bool& Usable, FString& State, TArray<FSJSessionResult>& SearchResults)
{
	if (SessionSearch.IsValid()) {
		switch (SessionSearch->SearchState) {
		case EOnlineAsyncTaskState::Type::NotStarted:
			State = "Not Started";
			break;
		case EOnlineAsyncTaskState::Type::InProgress:
			State = "In Progress";
			break;
		case EOnlineAsyncTaskState::Type::Done:
			State = "Done";
			break;
		case EOnlineAsyncTaskState::Type::Failed:
			State = "Failed";
			break;
		}


		TArray<FOnlineSessionSearchResult> SearchResultsToCast = SessionSearch->SearchResults;
		SearchResults = TArray<FSJSessionResult>();
		int32 i = 0;
		for (FOnlineSessionSearchResult& SearchResultToCast : SearchResultsToCast) {
			FBlueprintSessionResult BPSearchResult;
			BPSearchResult.OnlineResult = SearchResultToCast;
			FSJSessionResult SearchResult;
			SearchResult.Result = BPSearchResult;
			SearchResultToCast.Session.SessionSettings.Get("PlayerType", SearchResult.CharacterAvailable);
			SearchResultToCast.Session.SessionSettings.Get("CustomSessionName", SearchResult.SessionName);
			SearchResult.SessionIndexInSearchResults = i++;
			SearchResults.Add(SearchResult);
		}

		Usable = (SessionSearch->SearchState == EOnlineAsyncTaskState::Done);
		
	}
	else {
		Usable = false;
	}
}

void USJGameInstance::BeginLoadingScreen(const FString& InMapName) {
	if (!IsRunningDedicatedServer()) {
		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = false;
		LoadingScreen.WidgetLoadingScreen = FLoadingScreenAttributes::NewTestLoadingScreenWidget();

		GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);

		if (GetWorld()) {
			UCustomGameViewportClient* GameViewportClient = Cast<UCustomGameViewportClient>(GetWorld()->GetGameViewport());
			if (GameViewportClient) {
				GameViewportClient->Fade(1, true);
			}
		}
	}
}

void USJGameInstance::EndLoadingScreen(UWorld* InLoadedWorld) {
	if (InLoadedWorld) {
		UCustomGameViewportClient* GameViewportClient = Cast<UCustomGameViewportClient>(InLoadedWorld->GetGameViewport());
		if (GameViewportClient) {
			GameViewportClient->Fade(2, false);
		}
	}
}