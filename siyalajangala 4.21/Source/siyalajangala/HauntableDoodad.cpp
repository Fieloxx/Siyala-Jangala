// Fill out your copyright notice in the Description page of Project Settings.


#include "HauntableDoodad.h"
#include "SJ_PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "Utils.h"
#include "UnrealNetwork.h"
#include "Engine.h"

// TODO REFACTOR !
AHauntableDoodad::AHauntableDoodad(): Super() {
	Init();
}

void AHauntableDoodad::Init() {
	PrimaryActorTick.bCanEverTick = true;

	DoodadMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoodadMesh"));
	RootComponent = DoodadMesh;

	// DoodadMesh need to be movable.
	DoodadMesh->SetMobility(EComponentMobility::Movable);
	DoodadMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	DoodadMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	// DoodadMesh->SetGenerateOverlapEvents(true);

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	SpringArm->SetupAttachment(Cast<USceneComponent>(DoodadMesh));
	SpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 20.0f), FRotator(45.0f, 0.0f, 0.0f));
	SpringArm->TargetArmLength = 30.f;
	SpringArm->bEnableCameraLag = false;
	SpringArm->CameraLagSpeed = 1.0f;

	// Create a camera and attach to our spring arm
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("ActualCamera"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);

	bReplicates = true;
	bReplicateMovement = true;

	CorruptionState = ECorruptionState::CS_None;
	// Enhanced collision
	DoodadMesh->SetAllUseCCD(true);
}

// Called when the game starts or when spawned
void AHauntableDoodad::BeginPlay() {
	Super::BeginPlay();

	// NEW
	if (Role == ROLE_Authority) {
		SetOwner(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	}

	// Children stay relative to their parent.
	FAttachmentTransformRules AttachmentTransformRules(
		EAttachmentRule::KeepRelative,
		EAttachmentRule::KeepRelative,
		EAttachmentRule::KeepRelative,
		true
	);

	SpringArm->AttachToComponent(DoodadMesh, AttachmentTransformRules);
	Camera->AttachToComponent(SpringArm, AttachmentTransformRules, USpringArmComponent::SocketName);
	
	//SetOwner(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	//Utils::SetOwnerToSpirit(this);
}

void AHauntableDoodad::OnRep_PossessionChanged() {

}

// Called every frame
void AHauntableDoodad::Tick(float DeltaTime) {
	APawn::Tick(DeltaTime);
}

// Called to bind functionality to input
void AHauntableDoodad::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	// ACharacter::SetupPlayerInputComponent(PlayerInputComponent);

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 20.0f, FColor::Yellow, TEXT(" AHauntableDoodad::SetupPlayerInputComponent"));
	}

	PlayerInputComponent->BindAction("Possess", IE_Pressed, this, &AHauntableDoodad::Unpossess);
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AHauntableDoodad::Action);

	PlayerInputComponent->BindAxis("Turn", this, &AHauntableDoodad::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AHauntableDoodad::AddControllerPitchInput);
}

void AHauntableDoodad::OnRep_CorruptionChanged() {
	if (GEngine) {
		if (Role < ROLE_Authority) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("      CLIENT purify cosmetic"));
		}
		else {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("      SERVER purify cosmetic"));
		}
	}
}

ECorruptionState AHauntableDoodad::GetCorruption() const {
	return CorruptionState;
}

bool AHauntableDoodad::SetCorruption_Validate(ECorruptionState c_state) {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, TEXT("SetCorruption called"));
	}
	return true;
}

void AHauntableDoodad::SetCorruption_Implementation(ECorruptionState c_state) {
	CorruptionState = c_state;
}

bool AHauntableDoodad::Unpossess_Validate() {
	return true;
}

void AHauntableDoodad::Unpossess_Implementation() {
	SetPossessed(false);
	ASJ_PlayerController* PC = Cast<ASJ_PlayerController>(GetController());
	PC->UnpossessDoodad();
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Unpossessing"));
	}
}

bool AHauntableDoodad::Action_Validate() {
	return true;
}

void AHauntableDoodad::Action_Implementation() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Action"));
	}
}

bool AHauntableDoodad::SetPossessed_Validate(bool p) {
	return true;
}

void AHauntableDoodad::SetPossessed_Implementation(bool p) {
	bPossessed = p;
	OnRep_PossessionChanged();
}

void AHauntableDoodad::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AHauntableDoodad, CorruptionState);
	DOREPLIFETIME(AHauntableDoodad, bPossessed);
	DOREPLIFETIME(AHauntableDoodad, DoodadMesh);
}
