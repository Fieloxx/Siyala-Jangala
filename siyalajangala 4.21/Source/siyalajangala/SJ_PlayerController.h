// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Spirit.h"
#include "HauntableDoodad.h"
#include "SJ_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SIYALAJANGALA_API ASJ_PlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ASJ_PlayerController();

protected:
	UPROPERTY(Replicated)
	ASpirit* Spirit;
	UPROPERTY(Replicated)
	AHauntableDoodad* PossessedDoodad;

public:
	UFUNCTION(Server, Reliable, WithValidation)
	void PossessDoodad(AHauntableDoodad* doodad);
	UFUNCTION(Server, Reliable, WithValidation)
	void UnpossessDoodad();

	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void RelocateSpiritBeforeUnpossess();

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const;
};
