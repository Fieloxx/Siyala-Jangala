// Fill out your copyright notice in the Description page of Project Settings.


#include "CorruptedDoodad.h"

ACorruptedDoodad::ACorruptedDoodad() : AConcreteDoodad() {
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ACorruptedDoodad::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ACorruptedDoodad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
