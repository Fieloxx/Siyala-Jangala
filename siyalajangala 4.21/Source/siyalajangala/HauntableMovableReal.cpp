// Fill out your copyright notice in the Description page of Project Settings.

#include "HauntableMovableReal.h"
#include "PickableRunestone.h"
#include "PickableScalingFruit.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "UnrealNetwork.h"

AHauntableMovableReal::AHauntableMovableReal() : AFlyingHauntableDoodad() {
	SetReplicateMovement(true);
}

void AHauntableMovableReal::BeginPlay() {
	Super::BeginPlay();
	
	ToggleGravity(true);
	BaseRotation = GetActorRotation();
	BasePosition = GetActorLocation();
	TogglePhysics(true);
}

void AHauntableMovableReal::OnRep_PossessionChanged() {
	Super::OnRep_PossessionChanged();
	if (bPossessed) {
		TogglePhysics(false);
	}
}

void AHauntableMovableReal::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

void AHauntableMovableReal::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// PlayerInputComponent->BindAction("Possess", IE_Pressed, this, &AHauntableMovableReal::Unpossess);
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AHauntableMovableReal::Action);

	/*PlayerInputComponent->BindAxis("MoveForward", this, &AHauntableMovableReal::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AHauntableMovableReal::MoveRight);*/
}

void AHauntableMovableReal::Action_Implementation() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Rotating to base rotation !"));
	}
	SetActorLocationAndRotation(BasePosition, BaseRotation);
}

void AHauntableMovableReal::Move_Implementation(const FVector& MovementInput) {
	Super::Move_Implementation(MovementInput);
	if (LastHitResult.bBlockingHit) {
		// Checking what kind of object we have
		if (LastHitResult.GetActor()->GetClass()->IsChildOf(APickableDoodad::StaticClass())) {
			APickableDoodad* HitDoodad = Cast<APickableDoodad>(LastHitResult.GetActor());
			UStaticMeshComponent* HitMesh = Cast<UStaticMeshComponent>(HitDoodad->DoodadMesh);
			HitMesh->AddForce(MovementInput*SimulatedMass*5000);
		}
	}
}

//void AHauntableMovableReal::MoveForward(float Value) {
//	MovementInput.X = FMath::Clamp<float>(Value, -1.0f, 1.0f);
//}
//
//void AHauntableMovableReal::MoveRight(float Value) {
//	RotateInput = FMath::Clamp<float>(Value, -1.0f, 1.0f);
//}

void AHauntableMovableReal::Unpossess_Implementation() {
	AHauntableDoodad::Unpossess_Implementation();
	if (!bPossessed) {
		TogglePhysics(true);
	}
}

void AHauntableMovableReal::ToggleGravity_Real() {
	DoodadMesh->SetEnableGravity(bGravity);
}

bool AHauntableMovableReal::ToggleGravity_Validate(bool bEnabled) {
	return true;
}

void AHauntableMovableReal::ToggleGravity_Implementation(bool bEnabled) {
	bGravity = bEnabled;
	ToggleGravity_Real();
}

void AHauntableMovableReal::TogglePhysics_Real() {
	DoodadMesh->SetSimulatePhysics(bPhysics);
	if (bPhysics) {
		DoodadMesh->SetMassOverrideInKg(NAME_None, SimulatedMass);
	}
}

bool AHauntableMovableReal::TogglePhysics_Validate(bool Enabled) {
	return true;
}

void AHauntableMovableReal::TogglePhysics_Implementation(bool bEnabled) {
	bPhysics = bEnabled;
	TogglePhysics_Real();
}

void AHauntableMovableReal::OnRep_GravityChanged() {
	ToggleGravity_Real();
}

void AHauntableMovableReal::OnRep_PhysicsChanged() {
	TogglePhysics_Real();
}

void AHauntableMovableReal::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AHauntableMovableReal, bGravity);
	DOREPLIFETIME(AHauntableMovableReal, bPhysics);
	DOREPLIFETIME(AHauntableMovableReal, SimulatedMass);
}