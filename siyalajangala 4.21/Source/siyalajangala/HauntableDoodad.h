#pragma once

#include "GameFramework/Character.h"
#include "Doodad.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "HauntableDoodad.generated.h"

UCLASS()
class SIYALAJANGALA_API AHauntableDoodad : public APawn, public IDoodad {
	GENERATED_BODY()

public:
	AHauntableDoodad();

protected:
	virtual void Init();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Enum, ReplicatedUsing = OnRep_CorruptionChanged)
		ECorruptionState CorruptionState;

	UPROPERTY(EditAnywhere, Replicated)
		UStaticMeshComponent* DoodadMesh;
	UPROPERTY(EditAnywhere)
		USpringArmComponent* SpringArm;
	UPROPERTY(EditAnywhere)
		UCameraComponent* Camera;

	UPROPERTY(ReplicatedUsing = OnRep_PossessionChanged)
	bool bPossessed;

	UFUNCTION()
	virtual void OnRep_PossessionChanged();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	virtual void OnRep_CorruptionChanged();

	virtual ECorruptionState GetCorruption() const override;

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void SetCorruption(ECorruptionState c_state) override;

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void Unpossess();

	UFUNCTION(Server, Reliable, WithValidation)
	virtual void Action();

	UFUNCTION(Server, Reliable, WithValidation)
	void SetPossessed(bool p);

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;
};
