#include "FIllableWell.h"
#include "PickableKey.h"
#include "HauntableWater.h"
#include "Engine.h"
#include "UnrealNetwork.h"

// Sets default values
AFIllableWell::AFIllableWell() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	WellMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Well Mesh"));
	RootComponent = WellMesh;

	FilledWellMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Filled Well Mesh"));
	FilledWellMesh->SetupAttachment(RootComponent);

	Key = nullptr;

	bReplicates = true;
	SetReplicates(true);
}

// Called when the game starts or when spawned
void AFIllableWell::BeginPlay() {
	AActor::BeginPlay();
	
	// Trying to detect a key if not given.
	TArray<AActor*> Keys;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APickableKey::StaticClass(), Keys);
	if (Keys.Num() > 0) {
		Key = Cast<APickableKey>(Keys[0]);
		
	}

	OriginalFillPosition = FilledWellMesh->GetRelativeTransform().GetLocation();
}

void AFIllableWell::OnRep_Filled() {
	if (bFilled) {
		FilledWellMesh->SetRelativeLocation(MaxFillPosition);
		FVector Position = FVector(
			Key->GetTransform().GetLocation().X,
			Key->GetTransform().GetLocation().Y,
			MaxFillPosition.Z + Key->GetComponentsBoundingBox().GetSize().Z + 50
		);
		Key->SetActorLocation(Position, false);
		Key->DoodadMesh->SetSimulatePhysics(true);
		Key->DoodadMesh->SetEnableGravity(true);
	}
}

// Called every frame
void AFIllableWell::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (bFilling) {
		TimeSinceFillBeginning += DeltaTime;
		if (TimeSinceFillBeginning > TimeToFill) {
			TimeSinceFillBeginning = TimeToFill;
			bFilling = false;
			bFilled = true;
			OnRep_Filled();
		}
		else {
			FVector OldPosition = FilledWellMesh->GetRelativeTransform().GetLocation();
			FilledWellMesh->SetRelativeLocation(FMath::Lerp(OriginalFillPosition, MaxFillPosition, TimeSinceFillBeginning / TimeToFill));
			if (Key) {
				Key->SetActorLocation(Key->GetActorLocation() + FilledWellMesh->GetRelativeTransform().GetLocation() - OldPosition);
			}
		}
	}
}

bool AFIllableWell::Fill_Validate(AHauntableWater* Water) {
	return true;
}

void AFIllableWell::Fill_Implementation(AHauntableWater* Water) {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Well::fill"));
	}
	if (!Key) {
		UE_LOG(LogTemp, Error, TEXT("No key has been detected for the well. This will not fill correctly."));
		return;
	}
	if (!bFilled) {
		bFilling = true;
	}
}

void AFIllableWell::GetLifetimeReplicatedProps(class TArray<class FLifetimeProperty, class FDefaultAllocator>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AFIllableWell, bFilled);
	DOREPLIFETIME(AFIllableWell, bFilling);
}