# Idées d'Énigmes pour le Level Design

## L'oeuf perché

Un oeuf est coincé dans une branche d'un arbre, celui-ci brille d'une lumière étrange. L'esprit le voit clairement mais il ne peut pas faire grand-chose avec. Peut-être que Jahda pourrait faire quelque chose.

Le but est donc d'attirer Jahda en dessous de l'oeuf (qui est dans l'arbre), ensuite l'esprit possède l'arbre et peut le secouer légèrement, ce qui a pour effet de faire tomber l'oeuf; Jahda est en dessous prête à le rattraper; ce qu'elle fait.

Le rôle de l'oeuf est ensuite à déterminer mais il peut être un élément clé de l'énigme comme un des éléments à rassembler pour effectuer une certaine tâche, débloquer une zone, finir le niveau... etc. 
